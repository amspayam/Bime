package ir.pkokabi.pdialog.databinding;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
public abstract class DialogGeneralBinding extends ViewDataBinding {
    public final android.support.v7.widget.AppCompatButton firstBtn;
    public final android.support.v7.widget.AppCompatButton secondBtn;
    public final android.support.v7.widget.AppCompatTextView titleTv;
    // variables
    protected DialogGeneralBinding(android.databinding.DataBindingComponent bindingComponent, android.view.View root_, int localFieldCount
        , android.support.v7.widget.AppCompatButton firstBtn
        , android.support.v7.widget.AppCompatButton secondBtn
        , android.support.v7.widget.AppCompatTextView titleTv
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.firstBtn = firstBtn;
        this.secondBtn = secondBtn;
        this.titleTv = titleTv;
    }
    //getters and abstract setters
    public static DialogGeneralBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static DialogGeneralBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static DialogGeneralBinding bind(android.view.View view) {
        return null;
    }
    public static DialogGeneralBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static DialogGeneralBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static DialogGeneralBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
}