package ir.pkokabi.pdialog.databinding;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
public abstract class ViewDatePickerBinding extends ViewDataBinding {
    public final android.support.v7.widget.AppCompatButton cancelBtn;
    public final android.support.v7.widget.AppCompatButton chooseTimeBtn;
    public final android.support.v7.widget.RecyclerView dateRv;
    public final android.widget.LinearLayout fragmentContainer;
    public final android.support.v7.widget.RecyclerView hourRv;
    // variables
    protected ViewDatePickerBinding(android.databinding.DataBindingComponent bindingComponent, android.view.View root_, int localFieldCount
        , android.support.v7.widget.AppCompatButton cancelBtn
        , android.support.v7.widget.AppCompatButton chooseTimeBtn
        , android.support.v7.widget.RecyclerView dateRv
        , android.widget.LinearLayout fragmentContainer
        , android.support.v7.widget.RecyclerView hourRv
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.cancelBtn = cancelBtn;
        this.chooseTimeBtn = chooseTimeBtn;
        this.dateRv = dateRv;
        this.fragmentContainer = fragmentContainer;
        this.hourRv = hourRv;
    }
    //getters and abstract setters
    public static ViewDatePickerBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewDatePickerBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewDatePickerBinding bind(android.view.View view) {
        return null;
    }
    public static ViewDatePickerBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewDatePickerBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewDatePickerBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
}