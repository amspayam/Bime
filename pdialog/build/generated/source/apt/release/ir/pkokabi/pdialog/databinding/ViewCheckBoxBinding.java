package ir.pkokabi.pdialog.databinding;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
public abstract class ViewCheckBoxBinding extends ViewDataBinding {
    public final android.support.v7.widget.AppCompatImageView addImgV;
    public final android.support.v7.widget.AppCompatImageView doneImgV;
    public final android.widget.RelativeLayout iconRly;
    public final android.widget.TextView titleTv;
    // variables
    protected ViewCheckBoxBinding(android.databinding.DataBindingComponent bindingComponent, android.view.View root_, int localFieldCount
        , android.support.v7.widget.AppCompatImageView addImgV
        , android.support.v7.widget.AppCompatImageView doneImgV
        , android.widget.RelativeLayout iconRly
        , android.widget.TextView titleTv
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.addImgV = addImgV;
        this.doneImgV = doneImgV;
        this.iconRly = iconRly;
        this.titleTv = titleTv;
    }
    //getters and abstract setters
    public static ViewCheckBoxBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewCheckBoxBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewCheckBoxBinding bind(android.view.View view) {
        return null;
    }
    public static ViewCheckBoxBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewCheckBoxBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewCheckBoxBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
}