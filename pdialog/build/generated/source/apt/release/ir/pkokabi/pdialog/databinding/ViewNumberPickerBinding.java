package ir.pkokabi.pdialog.databinding;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
public abstract class ViewNumberPickerBinding extends ViewDataBinding {
    public final android.support.v7.widget.AppCompatImageButton minusImgBtn;
    public final android.widget.TextView numberTv;
    public final android.support.v7.widget.AppCompatImageButton plusImgBtn;
    public final android.widget.TextView titleTv;
    // variables
    protected ViewNumberPickerBinding(android.databinding.DataBindingComponent bindingComponent, android.view.View root_, int localFieldCount
        , android.support.v7.widget.AppCompatImageButton minusImgBtn
        , android.widget.TextView numberTv
        , android.support.v7.widget.AppCompatImageButton plusImgBtn
        , android.widget.TextView titleTv
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.minusImgBtn = minusImgBtn;
        this.numberTv = numberTv;
        this.plusImgBtn = plusImgBtn;
        this.titleTv = titleTv;
    }
    //getters and abstract setters
    public static ViewNumberPickerBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewNumberPickerBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewNumberPickerBinding bind(android.view.View view) {
        return null;
    }
    public static ViewNumberPickerBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewNumberPickerBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewNumberPickerBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
}