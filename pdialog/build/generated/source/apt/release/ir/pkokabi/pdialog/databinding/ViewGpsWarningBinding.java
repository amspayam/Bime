package ir.pkokabi.pdialog.databinding;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
public abstract class ViewGpsWarningBinding extends ViewDataBinding {
    public final android.support.v7.widget.AppCompatButton cancelACBtn;
    public final android.support.v7.widget.AppCompatButton confirmACBtn;
    // variables
    protected ViewGpsWarningBinding(android.databinding.DataBindingComponent bindingComponent, android.view.View root_, int localFieldCount
        , android.support.v7.widget.AppCompatButton cancelACBtn
        , android.support.v7.widget.AppCompatButton confirmACBtn
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.cancelACBtn = cancelACBtn;
        this.confirmACBtn = confirmACBtn;
    }
    //getters and abstract setters
    public static ViewGpsWarningBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewGpsWarningBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewGpsWarningBinding bind(android.view.View view) {
        return null;
    }
    public static ViewGpsWarningBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewGpsWarningBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewGpsWarningBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
}