package ir.pkokabi.pdialog.databinding;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
public abstract class ViewInternetWarningBinding extends ViewDataBinding {
    public final android.support.v7.widget.AppCompatButton gprsACBtn;
    public final android.support.v7.widget.AppCompatButton wifiACBtn;
    // variables
    protected ViewInternetWarningBinding(android.databinding.DataBindingComponent bindingComponent, android.view.View root_, int localFieldCount
        , android.support.v7.widget.AppCompatButton gprsACBtn
        , android.support.v7.widget.AppCompatButton wifiACBtn
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.gprsACBtn = gprsACBtn;
        this.wifiACBtn = wifiACBtn;
    }
    //getters and abstract setters
    public static ViewInternetWarningBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewInternetWarningBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewInternetWarningBinding bind(android.view.View view) {
        return null;
    }
    public static ViewInternetWarningBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewInternetWarningBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewInternetWarningBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
}