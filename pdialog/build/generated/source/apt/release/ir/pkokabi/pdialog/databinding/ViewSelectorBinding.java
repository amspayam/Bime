package ir.pkokabi.pdialog.databinding;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
public abstract class ViewSelectorBinding extends ViewDataBinding {
    public final android.support.v7.widget.AppCompatImageView iconImgv;
    public final android.support.v7.widget.AppCompatImageView selectorImgv;
    public final android.widget.RelativeLayout selectorRly;
    public final android.widget.TextView textView;
    // variables
    protected ViewSelectorBinding(android.databinding.DataBindingComponent bindingComponent, android.view.View root_, int localFieldCount
        , android.support.v7.widget.AppCompatImageView iconImgv
        , android.support.v7.widget.AppCompatImageView selectorImgv
        , android.widget.RelativeLayout selectorRly
        , android.widget.TextView textView
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.iconImgv = iconImgv;
        this.selectorImgv = selectorImgv;
        this.selectorRly = selectorRly;
        this.textView = textView;
    }
    //getters and abstract setters
    public static ViewSelectorBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewSelectorBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewSelectorBinding bind(android.view.View view) {
        return null;
    }
    public static ViewSelectorBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewSelectorBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewSelectorBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
}