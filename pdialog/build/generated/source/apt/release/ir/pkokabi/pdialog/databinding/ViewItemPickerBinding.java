package ir.pkokabi.pdialog.databinding;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
public abstract class ViewItemPickerBinding extends ViewDataBinding {
    public final android.support.v7.widget.AppCompatButton cancelBtn;
    public final android.support.v7.widget.AppCompatButton chooseItemBtn;
    public final android.support.v7.widget.RecyclerView itemRv;
    // variables
    protected ViewItemPickerBinding(android.databinding.DataBindingComponent bindingComponent, android.view.View root_, int localFieldCount
        , android.support.v7.widget.AppCompatButton cancelBtn
        , android.support.v7.widget.AppCompatButton chooseItemBtn
        , android.support.v7.widget.RecyclerView itemRv
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.cancelBtn = cancelBtn;
        this.chooseItemBtn = chooseItemBtn;
        this.itemRv = itemRv;
    }
    //getters and abstract setters
    public static ViewItemPickerBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewItemPickerBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ViewItemPickerBinding bind(android.view.View view) {
        return null;
    }
    public static ViewItemPickerBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewItemPickerBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    public static ViewItemPickerBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
}