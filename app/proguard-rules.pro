#Gson===============================================================================================
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
# Gson specific classes
-keepattributes Signature
-keepattributes *Annotation*
-keep class sun.misc.Unsafe { *; }
# Application classes that will be serialized/deserialized over Gson
-keep class mypersonalclass.data.model.** { *; }
#Picasso============================================================================================
-dontwarn com.squareup.okhttp.**
#Retrofit & OkHttp==================================================================================
-dontwarn okio.**
-dontwarn javax.annotation.Nullable
-dontwarn javax.annotation.ParametersAreNonnullByDefault
-dontwarn javax.annotation.**
-dontwarn retrofit2.Platform$Java8
#Apache Commons=====================================================================================
-dontwarn org.apache.commons.logging.**
-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**
-keep class android.net.http.** { *; }
-dontwarn android.net.http.**
#Rey Material=======================================================================================
-keep class com.rey.material.** { *; }
-dontwarn com.rey.material.**
##Models============================================================================================
-keep class ir.ottoshop.client.MasterPOJO.** {*;}
-keep class ir.ottoshop.client.Api.ModelServer.** {*;}
-keep class ir.ottoshop.client.UI.Activities.Car.CarBrand.Model.POJO.** {*;}
-keep class ir.ottoshop.client.UI.Activities.Address.AddressCedar.Model.POJO.** {*;}
-keep class ir.ottoshop.client.UI.Activities.Address.CedarPlace.Model.POJO.** {*;}
##Fabric============================================================================================
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**
##Loading===========================================================================================
-keep class com.wang.avi.** { *; }
-keep class com.wang.avi.indicators.** { *; }
#Cedar Map==========================================================================================
-dontwarn okhttp3.**
-keep class microsoft.mappoint.** {*;}
-keep class org.osmdroid.** {*;}
-dontwarn org.osmdroid.tileprovider.modules.NetworkAvailabliltyCheck
-keep class org.metalev.multitouch.controller.** {*;}