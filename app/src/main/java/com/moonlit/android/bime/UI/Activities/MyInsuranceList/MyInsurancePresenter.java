package com.moonlit.android.bime.UI.Activities.MyInsuranceList;

import android.content.Context;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.moonlit.android.bime.Api.ApiClient;
import com.moonlit.android.bime.Api.ApiName.GeneralApiInterface;
import com.moonlit.android.bime.Api.ConstantsMethodServer;
import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.Api.ServerResponse;
import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.Model.POJO.InsuranceModel;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.View.MyInsuranceView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoseinmotlagh on 10/19/17.
 */

public class MyInsurancePresenter implements ServerListener {
    private Context context;
    private MyInsuranceView myInsuranceView;

    public MyInsurancePresenter(Context context, MyInsuranceView myInsuranceView) {
        this.context = context;
        this.myInsuranceView = myInsuranceView;
    }

    public void getAllBime() {
        new ServerResponse()
                .setCall(new IdentifyModel(ConstantsMethodServer.getFirstServerData), ApiClient.getClient()
                        .create(GeneralApiInterface.class)
                        .getMyBime(), this);
    }

    @Override
    public void onSuccess(IdentifyModel identify, JsonObject data) {

        ArrayList<InsuranceModel> mainModels = new GsonBuilder()
                .serializeNulls()
                .create()
                .fromJson(data.getAsJsonArray("Result"), new TypeToken<List<InsuranceModel>>() {
                }.getType());
        myInsuranceView.onInsuranceDataLoad(mainModels);



        Log.e("ijijijijijiji", mainModels.get(0).getColorCode());
    }


    @Override
    public void onFailure(IdentifyModel identify, String error) {

    }
}
