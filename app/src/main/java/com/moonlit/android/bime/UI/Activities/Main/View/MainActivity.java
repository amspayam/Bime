package com.moonlit.android.bime.UI.Activities.Main.View;

import android.Manifest;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Toast;

import com.moonlit.android.bime.Master.CActivity;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.Accident.View.AccidentActivity;
import com.moonlit.android.bime.UI.Activities.BuyInsurance.BuyInsuranceActivity;
import com.moonlit.android.bime.UI.Activities.Dynamic.View.DynamicActivity;
import com.moonlit.android.bime.UI.Activities.Inquiry.View.InquiryActivity;
import com.moonlit.android.bime.UI.Activities.Main.Model.ListModel;
import com.moonlit.android.bime.UI.Activities.Main.Peresent.MainPresenter;
import com.moonlit.android.bime.UI.Activities.Messages.MessagesActivity;
import com.moonlit.android.bime.databinding.ActivityMainBinding;
import com.tbruyelle.rxpermissions.RxPermissions;

import net.gotev.speech.GoogleVoiceTypingDisabledException;
import net.gotev.speech.Speech;
import net.gotev.speech.SpeechDelegate;
import net.gotev.speech.SpeechRecognitionNotAvailable;

import java.util.ArrayList;
import java.util.List;

import io.rmiri.skeleton.Master.SkeletonConfig;
import ir.pkokabi.alertview.AlertView;
import ir.pkokabi.pdialogs.DialogGeneral.InternetWarning;

public class MainActivity extends CActivity implements MainViewInterface, SpeechDelegate {

    private ActivityMainBinding binding;
    private MainPresenter mainPresenter;

    private boolean isOpenVoice;
    private boolean isAnimationPlay;

    private BottomSheetBehavior behavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initialActivity();
        initialDataActivity();


        ArrayList<ListModel> lastSeen = new ArrayList<>();


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        binding.recyclerView.setHasFixedSize(true);
        LastSeenAdapter2 lasetSennAdapter2 = new LastSeenAdapter2(getApplicationContext(), lastSeen, new SkeletonConfig().build());
        binding.recyclerView.setAdapter(lasetSennAdapter2);

        //after 5 second get data fake
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lastSeen.add(new ListModel("“تصادف کردم”",633806));
                lastSeen.add(new ListModel("“بیمه عمرم می\u200Cخوام”",503939));
                lastSeen.add(new ListModel("“بیمه ماشینم تموم شده”",83638));
                lastSeen.add(new ListModel("“بیمه آتش سوزی خوب می\u200Cخوام”",986588));
                lastSeen.add(new ListModel("“بیمه آرمان می\u200Cخوام”",4545));
                lasetSennAdapter2.addMoreDataAndSkeletonFinish(lastSeen);
            }
        }, 1500);


        binding.backGroundRedVoiceRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public void networkWarning(boolean showRetry) {
        new InternetWarning(context);
    }

    @Override
    public void showError(String error) {
        new AlertView(context, error, AlertView.STATE_ERROR);
    }

    @Override
    public void showMessage(String message) {
        new AlertView(context, message, AlertView.STATE_SUCCESS);
    }

    @Override
    public void aiSpeechShow(int type) {

        binding.voiceACImgV.setVisibility(View.VISIBLE);
        binding.brainACImgV.setVisibility(View.GONE);

        switch (type) {
            case 468662:
                //kharide bime badane
                startActivity(new Intent(context, DynamicActivity.class)
                        .putExtra("title", "خرید بیمه بدنه"));
                break;
            case 617821:
            case 503939:
                //kharide bime omr
                startActivity(new Intent(context, DynamicActivity.class)
                        .putExtra("title", "خرید بیمه عمر"));
                break;
            case 90068:
                //kharide bime mosaferati
                startActivity(new Intent(context, DynamicActivity.class)
                        .putExtra("title", "خرید بیمه مسافرتی"));
                break;
            case 633806:
                //sos
                startActivity(new Intent(context, AccidentActivity.class));
                break;
            case 986588:
                //kharide bime atashsozi
                startActivity(new Intent(context, DynamicActivity.class)
                        .putExtra("title", "خرید بیمه آتش سوزی"));
                break;
            case 83638:
                //kharide bime
                startActivity(new Intent(context, BuyInsuranceActivity.class));
                break;
            case 306934:
                //kharide bime masoliayte pezeshki
                startActivity(new Intent(context, DynamicActivity.class)
                        .putExtra("title", "خرید بیمه مسولیت پزشکی"));
                break;case 4545:
                //kharide bime masoliayte pezeshki
                startActivity(new Intent(context, DynamicActivity.class)
                        .putExtra("title", "خرید بیمه آرمان"));
                break;
            case 635004:
                //afzayesh etrab
                showMessage("افزایش اعتبار");
                break;
            case 999851:
                //kharide bime shakhse sales
                startActivity(new Intent(context, DynamicActivity.class)
                        .putExtra("title", "خرید بیمه شخص ثالث"));
                break;
            case 358154:
                //estelame khesarat
                startActivity(new Intent(context, InquiryActivity.class));
            case 461634:
                //soal az karshenas
                showMessage("هدایت به سمت چت بات کارشناس");
                break;
            default:
                new AlertView(context, "پاسخی برای سوال شما در سیستم پیدا نشد، در اسرع وقت کارشناسان ما از طریق نرم افزار پاسخ شما را می دهند"
                        , AlertView.STATE_WARNING);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.voiceRly:
                if (Speech.getInstance().isListening()) {
                    Speech.getInstance().stopListening();
                } else {
                    RxPermissions.getInstance(this)
                            .request(Manifest.permission.RECORD_AUDIO)
                            .subscribe(granted -> {
                                if (granted) { // Always true pre-M
                                    onRecordAudioPermissionGranted();
                                    if (!isOpenVoice) {
                                        openVoice();
                                    }
                                } else {
                                    Toast.makeText(MainActivity.this, R.string.permission_required, Toast.LENGTH_LONG);
                                }
                            });
                }
                break;
            case R.id.closeACImgB:
                openVoice();
                break;
            case R.id.menuACImgB:

                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                MenuFragment menuFragment = MenuFragment.newInstance();
                FragmentManager fragmentManager = getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction transation = fragmentManager.beginTransaction();
                transation.replace(R.id.reminder_rl, menuFragment, "menu");
                transation.commit();
                fragmentManager.executePendingTransactions();

                break;

            case R.id.maskFly:
                closeShit();
                break;
            case R.id.notificationACImgB:
                startActivity(new Intent(getApplicationContext(), MessagesActivity.class));
                break;
            case R.id.accidentLy:
                startActivity(new Intent(getApplicationContext(), AccidentActivity.class));
                break;

        }
    }

    @Override
    protected void initialDataActivity() {
        super.initialDataActivity();

        mainPresenter = new MainPresenter(context, this);


        int[] colors = {
                Color.parseColor("#2B86C5"),
                Color.parseColor("#6065B2"),
                Color.parseColor("#9543A0"),
                Color.parseColor("#CA228D"),
                Color.parseColor("#FF007A")
        };


        binding.progress.setColors(colors);

        behavior = BottomSheetBehavior.from(binding.reminderRl);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");

                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");

                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        //binding.reminderFl.setVisibility(View.GONE);
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");

                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        // binding.reminderFl.setVisibility(View.GONE);
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                binding.maskFly.setAlpha(slideOffset);
                Log.i("BottomSheetCallback", "called" + slideOffset);
                binding.voiceRly.setScaleX((float) (1.0f - (0.1 * slideOffset)));
                binding.voiceRly.setScaleY((float) (1.0f - (0.1 * slideOffset)));
                binding.backGroundRedVoiceRly.setScaleX((float) (1.0f - (0.1 * slideOffset)));
                binding.backGroundRedVoiceRly.setScaleY((float) (1.0f - (0.1 * slideOffset)));
                binding.headTv.setScaleX((float) (1.0f - (0.1 * slideOffset)));
                binding.headTv.setScaleY((float) (1.0f - (0.1 * slideOffset)));

                if (slideOffset > 0.0f) {
                    binding.maskFly.setVisibility(View.VISIBLE);
                } else {
                    binding.maskFly.setVisibility(View.GONE);
                }
            }
        });


        setListenerForViews();
    }

    @Override
    public void setListenerForViews() {
        binding.voiceRly.setOnClickListener(this);
        binding.closeACImgB.setOnClickListener(this);
        binding.menuACImgB.setOnClickListener(this);
        binding.maskFly.setOnClickListener(this);
        binding.notificationACImgB.setOnClickListener(this);
        binding.accidentLy.setOnClickListener(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Speech.getInstance().shutdown();
    }

    void openVoice() {

        if (isAnimationPlay)
            return;

        if (isOpenVoice) {
            isOpenVoice = false;
            Animation anim = new ScaleAnimation(8.0f, 1.0f, 8.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(525);
            anim.setFillAfter(true);
            binding.backGroundRedVoiceRly.startAnimation(anim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    isAnimationPlay = true;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    isAnimationPlay = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            binding.headTv.setText("چه کمکی از دستم برمیاد؟");
            binding.headTv.setTextColor(ContextCompat.getColor(context, R.color.textColorPrimary));

            binding.closeACImgB.setVisibility(View.GONE);
            binding.accidentLy.setEnabled(true);

            binding.vocieResultTv.setText("");
            binding.vocieResultTv.setVisibility(View.GONE);

        } else {
            isOpenVoice = true;

            Animation anim = new ScaleAnimation(1.0f, 8.0f, 1.0f, 8.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(525);
            anim.setFillAfter(true);
            binding.backGroundRedVoiceRly.startAnimation(anim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    isAnimationPlay = true;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    isAnimationPlay = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            binding.headTv.setText("لطفا سوال خود را بپرسید");
            binding.headTv.setTextColor(ContextCompat.getColor(context, R.color.white));

            binding.closeACImgB.setVisibility(View.VISIBLE);
            binding.accidentLy.setEnabled(false);

            binding.vocieResultTv.setText("");
            binding.vocieResultTv.setVisibility(View.VISIBLE);

        }
    }


    private void onRecordAudioPermissionGranted() {
        binding.voiceACImgV.setVisibility(View.GONE);
        binding.linearLayout.setVisibility(View.VISIBLE);

        try {
            Speech.getInstance().stopTextToSpeech();
            Speech.getInstance().startListening(binding.progress, MainActivity.this);

        } catch (SpeechRecognitionNotAvailable exc) {
        } catch (GoogleVoiceTypingDisabledException exc) {
        }
    }

    private void onSpeakClick() {
//        if (textToSpeech.getText().toString().trim().isEmpty()) {
//            Toast.makeText(this, R.string.input_something, Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        Speech.getInstance().say(textToSpeech.getText().toString().trim(), new TextToSpeechCallback() {
//            @Override
//            public void onStart() {
//                Toast.makeText(MainActivity.this, "TTS onStart", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onCompleted() {
//                Toast.makeText(MainActivity.this, "TTS onCompleted", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onError() {
//                Toast.makeText(MainActivity.this, "TTS onError", Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public void onStartOfSpeech() {
    }

    @Override
    public void onSpeechRmsChanged(float value) {
        Log.d(getClass().getSimpleName(), "Speech recognition rms is now " + value + "dB");
    }

    @Override
    public void onSpeechResult(String result) {

        binding.voiceACImgV.setVisibility(View.VISIBLE);
        binding.linearLayout.setVisibility(View.GONE);

        if (!result.isEmpty()) {
            mainPresenter.aiSpeech(binding.vocieResultTv.getText().toString());
            binding.voiceACImgV.setVisibility(View.GONE);
            binding.brainACImgV.setVisibility(View.VISIBLE);
        }

        Log.i("%%", result);
//        text.setText(result);
//        Toast.makeText(context, "  " + result, Toast.LENGTH_SHORT).show();
        if (result.isEmpty()) {
            Speech.getInstance().say(getString(R.string.repeat));
            openVoice();
        } else {

            Log.i("%%", result);
//            Speech.getInstance().say(result);
        }
    }

    @Override
    public void onBackPressed() {

        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else
            super.onBackPressed();
    }

    @Override
    public void onSpeechPartialResults(List<String> results) {
//        text.setText("");
//        for (String partial : results) {
//            text.append(partial + " ");
//        }
        StringBuilder stb = new StringBuilder();
        for (String partial : results) {
            stb.append(partial + " ");
        }

        Log.i("%%%%%%%%%%%%%%%%%&", stb.toString());


        binding.vocieResultTv.setText(stb.toString());
//        Toast.makeText(context, "  " + stb, Toast.LENGTH_SHORT).show();
    }


    public void closeShit() {
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

}
