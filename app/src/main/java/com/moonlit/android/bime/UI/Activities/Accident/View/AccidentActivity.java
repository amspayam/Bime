package com.moonlit.android.bime.UI.Activities.Accident.View;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.moonlit.android.bime.Master.CActivity;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.Accident.Model.POJO.AddSOSMs;
import com.moonlit.android.bime.UI.Activities.Accident.Peresent.AccidentPresenter;
import com.moonlit.android.bime.UI.Activities.AccidentConfirm.View.AccidentConfirmActivity;
import com.moonlit.android.bime.Utils.Constants;
import com.moonlit.android.bime.Utils.GPSTracker;
import com.moonlit.android.bime.Utils.Logger.CLog;
import com.moonlit.android.bime.Utils.NetworkUtils;
import com.moonlit.android.bime.Utils.SmsSender;
import com.moonlit.android.bime.databinding.ActivityAccidentBinding;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import ir.pkokabi.alertview.AlertView;
import ir.pkokabi.pdialogs.DialogGeneral.DialogGeneral;
import ir.pkokabi.pdialogs.DialogGeneral.InternetWarning;

import static com.moonlit.android.bime.Utils.Constants.SEND_SMS;

public class AccidentActivity extends CActivity implements AccidentViewInterface
        , View.OnLongClickListener, OnMapReadyCallback, View.OnTouchListener {

    ActivityAccidentBinding binding;
    AccidentPresenter accidentPresenter;

    /*Activity Values*/
    String googleAdd = "";
    boolean fromSetting;
    GoogleMap map;
    GPSTracker gps;
    LatLng latlng;
    Marker marker;
    String[] permissionMap = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    public static short ZOOM_LEVEL = 17;
    private boolean hasVoice;

    /*SendVoice*/
    Handler timerHandler = new Handler();
    private boolean isRecordingStopped = true;
    private int seconds, minutes;
    private long milliSecondTime, startTime, timeBuff, updateTime = 0L;
    private static final String TIMER_FORMAT = "%02d";

    /*VoiceRecording Values*/
    private MediaRecorder recorder = null;
    String[] permissionVoice = {Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    /*SMS*/
    SmsSender smsSender = new SmsSender();
    String[] permissionSMS = {Manifest.permission.SEND_SMS};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_accident);

        initialActivity();
        initialDataActivity();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                binding.uploadPhotoImgv.setColorFilter(ContextCompat.getColor(context, R.color.accentColor));
                binding.uploadPhotoBtn.setBackgroundResource(R.drawable.shape_accent_32);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fromSetting) {
            locationPermissionCheck();
            fromSetting = false;
        }
        if (marker != null)
            marker.remove();
    }

    @Override
    public void networkWarning(boolean showRetry) {
        binding.sendEmergencyBtn.stopBtnLoading();
        new InternetWarning(context);
    }

    @Override
    public void showError(String message) {
        binding.sendEmergencyBtn.stopBtnLoading();
        new AlertView(context, message, AlertView.STATE_ERROR);
    }

    @Override
    public void showMessage(String message) {
        binding.sendEmergencyBtn.stopBtnLoading();
        new AlertView(context, message, AlertView.STATE_SUCCESS);
    }

    @Override
    public void sosId(String id, AddSOSMs sosModel) {
        finish();
        binding.sendEmergencyBtn.stopBtnLoading();
        startActivity(new Intent(context, AccidentConfirmActivity.class)
                .putExtra("sos", new Gson().toJson(sosModel)));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        locationPermissionCheck();
    }

    @Override
    public boolean onLongClick(View v) {
        voicePermissionCheck();
        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (isRecordingStopped)
                    voicePermissionCheck();
                return true;
            case MotionEvent.ACTION_UP:
                if (!isRecordingStopped)
                    new Handler().postDelayed(this::stopRecording, 200);
                return true;
            case MotionEvent.AXIS_SIZE:
                if (!isRecordingStopped)
                    new Handler().postDelayed(this::stopRecording, 200);
                return true;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.deleteImgv:
                if (isRecordingStopped)
                    new DialogGeneral(context, getString(R.string.titleDelete), getString(R.string.delete)
                            , getString(R.string.cancel)) {
                        @Override
                        public void onConfirm() {
                            if (isRecordingStopped) {
                                if (new File(getFilename()).delete())
                                    isRecordingStopped = true;
                                startTime = 0L;
                                timeBuff = 0L;
                                updateTime = 0L;
                                seconds = 0;
                                minutes = 0;
                                recordingView(true);
                            }
                        }
                    };
                break;
            case R.id.sendEmergencyBtn:
                if (new NetworkUtils(context).isOnline()) {
                    binding.sendEmergencyBtn.startBtnLoading();
                    accidentPresenter.addSOS(new AddSOSMs(binding.policeCs.isSelected(),
                            binding.ambulanceCs.isSelected(), binding.fireFightingCs.isSelected()
                            , binding.craneCs.isSelected(), binding.agentCs.isSelected(), binding.descEdt.getEditTextInput()
                            , gps.getLatitude(), gps.getLongitude()));
                } else {
                    binding.sendEmergencyBtn.startBtnLoading();
                    smsPermissionCheck();
                }
                break;
            case R.id.uploadPhotoBtn:
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(this);
                break;
        }
    }

    @Override
    protected void initialDataActivity() {
        super.initialDataActivity();

        accidentPresenter = new AccidentPresenter(context, this);
        toolBarConfig(BACK, DEFAULT_COLOR, "کمک فوری");
        setListenerForViews();

        hideStatuesBar();

        if (new NetworkUtils(context).isOnline())
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap)).getMapAsync(this);
        else {
            gps = new GPSTracker(context);
            if (!gps.canGetLocation())
                gps.showSettingsAlert();
            binding.uploadPhotoBtn.setBackgroundResource(R.drawable.shape_disable_32);
            binding.uploadPhotoImgv.setColorFilter(ContextCompat.getColor(context, R.color.disableColor));
            binding.uploadVoiceBtn.setBackgroundResource(R.drawable.shape_disable_32);
            binding.uploadVoiceImgv.setColorFilter(ContextCompat.getColor(context, R.color.disableColor));
            binding.uploadPhotoBtn.setEnabled(false);
            binding.uploadVoiceBtn.setEnabled(false);
        }
        binding.mapRly.getLayoutParams().height = (int) (0.25 * height);
    }

    @Override
    public void setListenerForViews() {
        binding.uploadVoiceBtn.setOnTouchListener(this);
        binding.uploadPhotoBtn.setOnClickListener(this);
        binding.sendEmergencyBtn.setOnClickListener(this);
        binding.deleteImgv.setOnClickListener(this);
    }

    private void locationPermissionCheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                if (ActivityCompat.checkSelfPermission(context, permissionMap[0]) != PackageManager.PERMISSION_GRANTED
                        | ActivityCompat.checkSelfPermission(context, permissionMap[1]) != PackageManager.PERMISSION_GRANTED)
                    requestPermissions(permissionMap, Constants.ACCESS_MAP);
                else
                    getLocation();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            getLocation();
    }

    private void getLocation() {
        if (map != null) {
            try {
                gps = new GPSTracker(context);
                if (gps.canGetLocation()) {
                    latlng = new LatLng(gps.getLatitude(), gps.getLongitude());
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, ZOOM_LEVEL));
                    map.getUiSettings().setScrollGesturesEnabled(false);
                } else
                    gps.showSettingsAlert();
            } catch (Exception e) {
                locationPermissionCheck();
            }
        } else
            showError(getString(R.string.installGooglePlay));
    }

    private MediaRecorder.OnErrorListener errorListener = (mr, what, extra) -> CLog.i("Audio", "Error: " + what + ", " + extra);

    private MediaRecorder.OnInfoListener infoListener = (mr, what, extra) -> CLog.i("Audio", "Warning: " + what + ", " + extra);

    private void voicePermissionCheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                if (ActivityCompat.checkSelfPermission(this, permissionVoice[0]) != PackageManager.PERMISSION_GRANTED
                        | ActivityCompat.checkSelfPermission(context, permissionVoice[1]) != PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(this, permissionVoice, Constants.RECORD_AUDIO);
                else
                    startRecording();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            startRecording();
    }

    /*Permission Check*/
    private void smsPermissionCheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                if (ActivityCompat.checkSelfPermission(context, permissionSMS[0]) != PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(this, permissionSMS, SEND_SMS);
                else
                    sendSms();
            } catch (Exception e) {
                CLog.i("====", e.toString());
            }
        } else
            sendSms();
    }

    private void sendSms() {
        ArrayList<String> sos = new ArrayList<>();
        sos.add(binding.policeCs.isSelectedString());
        sos.add(binding.ambulanceCs.isSelectedString());
        sos.add(binding.fireFightingCs.isSelectedString());
        sos.add(binding.craneCs.isSelectedString());
        sos.add(binding.agentCs.isSelectedString());
        sos.add(binding.descEdt.getEditTextInput());
        sos.add(String.valueOf(gps.getLatitude()));
        sos.add(String.valueOf(gps.getLongitude()));
        smsSender.sendSMS(context, sos.toString());
        binding.sendEmergencyBtn.stopBtnLoading();
        new AlertView(context,"پیام شما با موفقیت به سامانه پیامکی ما ارسال شد.",AlertView.STATE_SUCCESS){
            @Override
            public void onFinish() {
                super.onFinish();
                binding.sendEmergencyBtn.stopBtnLoading();
                finish();
            }
        };
    }

    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, getString(R.string.app_name));

        if (!file.exists())
            file.mkdirs();

        return (file.getAbsolutePath() + "/"
                + "Bime_" + Calendar.getInstance().getTimeInMillis() + Constants.AUDIO_RECORDER_FILE_EXT_MP3);
    }

    private void startRecording() {
        isRecordingStopped = false;
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(getFilename());
        recorder.setOnErrorListener(errorListener);
        recorder.setOnInfoListener(infoListener);
        recordingView(true);
        startTime = SystemClock.uptimeMillis();
        timerHandler.postDelayed(timerTask, 0);
        try {
            recorder.prepare();
            recorder.start();
            binding.deleteImgv.setVisibility(View.INVISIBLE);
        } catch (IllegalStateException | IOException e) {
            e.printStackTrace();
        }
    }

    private void stopRecording() {
        if (recorder != null) {
            isRecordingStopped = true;
            recorder.stop();
            recorder.reset();
            recorder.release();
            recorder = null;
            timeBuff += milliSecondTime;
            timerHandler.removeCallbacks(timerTask);
            recordingView(false);
        }
    }

    private void recordingView(boolean isReset) {
        if (isReset) {
            milliSecondTime = 0L;
            startTime = 0L;
            timeBuff = 0L;
            updateTime = 0L;
            seconds = 0;
            minutes = 0;
            binding.timerTv.setText("");
            binding.deleteImgv.setVisibility(View.INVISIBLE);
            binding.uploadVoiceImgv.setImageResource(R.drawable.ic_voice_record);
            binding.uploadVoiceBtn.setBackgroundResource(R.drawable.shape_blue_32);
            hasVoice = false;
        } else {
            binding.deleteImgv.setVisibility(View.VISIBLE);
            binding.uploadVoiceImgv.setImageResource(R.drawable.ic_voice_record_selected);
            binding.uploadVoiceBtn.setBackgroundResource(R.drawable.shape_accent_32);
            hasVoice = true;
        }
    }

    public Runnable timerTask = new Runnable() {

        public void run() {

            milliSecondTime = SystemClock.uptimeMillis() - startTime;

            updateTime = timeBuff + milliSecondTime;

            seconds = (int) (updateTime / 1000);

            minutes = seconds / 60;

            seconds = seconds % 60;

            binding.timerTv.setText(String.valueOf(minutes + ":" + String.format(TIMER_FORMAT, seconds)));

            timerHandler.postDelayed(this, 0);
        }

    };

}