package com.moonlit.android.bime.UI.Activities.MyInsuranceList;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v4.app.Fragment;
import android.view.ViewGroup;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.Model.POJO.InsuranceModel;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.View.MyInsuranceActivity;
import com.moonlit.android.bime.databinding.DialogAutoPaidBinding;

/**
 * Created by hoseinmotlagh on 10/20/17.
 */

public class AutoPaidDialog extends Fragment implements
        android.view.View.OnClickListener {


    private DialogAutoPaidBinding binding;


    public static AutoPaidDialog newInstance(InsuranceModel insuranceModel) {

        Bundle args = new Bundle();
        args.putSerializable("insuranceModel", insuranceModel);
        AutoPaidDialog fragment = new AutoPaidDialog();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_auto_paid, container, false);
        View view = binding.getRoot();
        InsuranceModel item = (InsuranceModel) getArguments().getSerializable("insuranceModel");
        binding.taiid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MyInsuranceActivity) getActivity()).cloasedialog();
            }
        });
        binding.laghv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MyInsuranceActivity) getActivity()).cloasedialog();
            }
        });


        binding.titleTv.setText(item.getTitle());
        binding.leftTimeTv.setText(item.getRestDays() + " روز مانده ");
        binding.numberTv.setText(item.getDescription());
        binding.typeTv.setText("");

        return view;


    }


    @Override
    public void onClick(View view) {

    }


}
