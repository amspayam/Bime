package com.moonlit.android.bime.Api;


import com.moonlit.android.bime.Api.ModelServer.ResponseBaseM;
import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.Utils.AppController;
import retrofit2.Call;
import retrofit2.Callback;

public class ServerResponse {

    public void setCall(final IdentifyModel identify, Call<ResponseBaseM> call, final ServerListener serverListener) {

        call.enqueue(new Callback<ResponseBaseM>() {

            @Override
            public void onResponse(Call<ResponseBaseM> call, retrofit2.Response<ResponseBaseM> response) {
                try {
                    if (response.code() == 200) {
                        if (new ErrorHandling().ErrorCode(response.body()).equals("OK"))
                            serverListener.onSuccess(identify, response.body().getData());
                         else
                            serverListener.onFailure(identify, new ErrorHandling()
                                    .ErrorCode(response.body()));
                    } else
                        serverListener.onFailure(identify, response.body().getStatusMessage());
                } catch (Exception e) {
                    serverListener.onFailure(identify, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseM> call, Throwable t) {
                serverListener.onFailure(identify, AppController.getInstance().getString(R.string.serverError));
            }

        });
    }

}
