package com.moonlit.android.bime.SampleAdapter;

import com.google.gson.annotations.SerializedName;

/**
 * Created by p.kokabi on 11/14/2016.
 */

public class ContentM {

    private String duration, price, videoRate;
    @SerializedName("videoID")
    private String idContent;
    @SerializedName("serviceID")
    private String idService;
    @SerializedName("service_name")
    private String serviceName;
    @SerializedName("thumb_path")
    private String videoThumb;
    @SerializedName("video_name")
    private String videoName;
    @SerializedName("ContentType_ID")
    private short contentType;
    @SerializedName("buy_status")
    private boolean buyStatus;

    public String getDuration() {
        return duration;
    }

    public String getPrice() {
        return price;
    }

    public String getVideoRate() {
        return videoRate;
    }

    public String getIdContent() {
        return idContent;
    }

    public String getIdService() {
        return idService;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getVideoThumb() {
        return videoThumb;
    }

    public String getVideoName() {
        return videoName;
    }

    public short getContentType() {
        return contentType;
    }

    public boolean isBuyStatus() {
        return buyStatus;
    }
}
