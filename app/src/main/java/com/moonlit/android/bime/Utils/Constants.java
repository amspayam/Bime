package com.moonlit.android.bime.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Constants {

    public static String URL_API = "http://217.20.112.152:8059/api/";
    public static String URL_API_GOOGLE = "https://maps.googleapis.com/maps/api/";

    /*Flavor*/
    public static final String GOOGLE_PLAY = "GooglePlay";
    public static final String CAFE_BAZAAR = "CafeBazaar";
    public static final String GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?id=";
    public static final String CAFE_BAZAAR_URL = "https://cafebazaar.ir/app/";

    /*Permissions*/
    public static int RECORD_AUDIO = 1;
    public static int ACCESS_MAP = 2;
    public static int SEND_SMS = 3;

    public static long CONNECT_TIMEOUT = 30;
    public static long READ_TIMEOUT = 30;
    public static long WRITE_TIMEOUT = 30;

    public static String RTL = "\u200F";
    public static String SPACE = "\u0020";
    public static String AUDIO_RECORDER_FILE_EXT_MP3 = ".mp3";

    public static void permissionSetting(Context context) {
        context.startActivity(new Intent().setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                .addCategory(Intent.CATEGORY_DEFAULT)
                .setData(Uri.parse("package:" + context.getPackageName()))
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                .addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS));
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window TOKEN from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window TOKEN from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}