package com.moonlit.android.bime.UI.Activities.Accident.Model;

import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.UI.Activities.Accident.Model.POJO.AddSOSMs;

interface AccidentModelInterface {

    void addSOSApi(AddSOSMs sosModel, ServerListener serverListener);

}
