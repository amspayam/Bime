package com.moonlit.android.bime.Components;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.databinding.CustomButtonLoadingBinding;

/**
 * Created by p.kokabi on 9/19/2017.
 */

public class CustomButtonLoading extends RelativeLayout {

    private CustomButtonLoadingBinding binding;
    Context context;
    private String titleButton;
    int buttonBackground, buttonTextColor, loadingColor;

    public CustomButtonLoading(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CustomButtonLoading(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    public CustomButtonLoading(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.generalAttrs);

        titleButton = typedArray.getString(R.styleable.generalAttrs_text);
        buttonBackground = typedArray.getResourceId(R.styleable.generalAttrs_buttonBackground, R.drawable.shape_accent_32);
        buttonTextColor = typedArray.getColor(R.styleable.generalAttrs_textColor, ContextCompat.getColor(context, R.color.white));
        loadingColor = typedArray.getColor(R.styleable.generalAttrs_loadingColor, ContextCompat.getColor(context, R.color.white));

        this.context = context;
        init();
        typedArray.recycle();

    }

    public void init() {
        if (isInEditMode())
            return;
        binding = DataBindingUtil
                .inflate(LayoutInflater.from(getContext()), R.layout.custom_button_loading, this, true);

        binding.btnRly.setBackgroundResource(buttonBackground);
        binding.button.setText(titleButton);
        binding.button.setTextColor(buttonTextColor);
        binding.buttonLoading.setIndicatorColor(loadingColor);

    }

    public void startBtnLoading() {
        if (binding.button.getVisibility() == VISIBLE) {
            binding.button.startAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_out));
            binding.button.setVisibility(INVISIBLE);
            binding.button.setEnabled(false);
            setEnabled(false);
            postDelayed(loadingDelayedShow, 400);
        }
    }

    public void stopBtnLoading() {
        if (binding.button.getVisibility() != VISIBLE)
            postDelayed(allViewDelayShow, 400);
    }

    private Runnable loadingDelayedShow = new Runnable() {

        @Override
        public void run() {
            binding.buttonLoading.smoothToShow();
            removeCallbacks(loadingDelayedShow);
        }
    };

    private Runnable buttonViewDelayedShow = new Runnable() {

        @Override
        public void run() {
            binding.button.startAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in));
            binding.button.setVisibility(VISIBLE);
            binding.button.setEnabled(true);
            removeCallbacks(buttonViewDelayedShow);
        }
    };


    private Runnable allViewDelayShow = new Runnable() {
        @Override
        public void run() {
            binding.buttonLoading.smoothToHide();
            postDelayed(buttonViewDelayedShow, 400);
            setEnabled(true);
        }
    };

}
