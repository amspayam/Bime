package com.moonlit.android.bime.Components;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputFilter;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.Utils.FontsOverride;
import com.moonlit.android.bime.databinding.CustomEditTextBinding;

public class CustomEditText extends LinearLayout {

    private CustomEditTextBinding binding;
    Context context;
    String editTextInput, hint, textViewInput;
    int inputType, textColor, hintColor, maxLength, gravity, imeOptions, textSize, tintColor;
    int lines, maxLines;

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    public CustomEditText(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOrientation(LinearLayout.VERTICAL);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        this.context = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.generalAttrs);
        editTextInput = typedArray.getString(R.styleable.generalAttrs_text);
        textColor = typedArray.getColor(R.styleable.generalAttrs_textColor, ContextCompat.getColor(context, R.color.textColorSecondary));
        hint = typedArray.getString(R.styleable.generalAttrs_hint);
        hintColor = typedArray.getColor(R.styleable.generalAttrs_hintColor, ContextCompat.getColor(context, R.color.lightGray));
        inputType = typedArray.getInt(R.styleable.generalAttrs_inputType, 0);
        maxLength = typedArray.getInt(R.styleable.generalAttrs_maxLength, 100);
        gravity = typedArray.getInt(R.styleable.generalAttrs_gravity, 0);
        lines = typedArray.getInt(R.styleable.generalAttrs_lines, 0);
        maxLines = typedArray.getInt(R.styleable.generalAttrs_maxLines, 0);
        imeOptions = typedArray.getInt(R.styleable.generalAttrs_imeOptions, 0);
        textSize = typedArray.getDimensionPixelSize(R.styleable.generalAttrs_textSizeCustom, 0);
        tintColor = typedArray.getInt(R.styleable.generalAttrs_tintColor, 0x000000);

        init();
        typedArray.recycle();

    }

    public void init() {
        if (isInEditMode())
            return;
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.custom_edit_text, this, true);

        binding.editText.setTextColor(textColor);
        binding.editText.setHint(hint);
        binding.editText.setHintTextColor(hintColor);
        binding.editText.setMaxLines(maxLines);
        if (maxLines > 1) {
            binding.editText.setSingleLine(false);
            binding.editText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            binding.editText.setLines(lines);
        } else {
            binding.editText.setInputType(inputType);
            binding.editText.setImeOptions(imeOptions);
            binding.editText.setLines(1);
        }
        binding.editText.setGravity(gravity);
        binding.editText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        if (inputType == 3)
            binding.editText.setKeyListener(DigitsKeyListener.getInstance("0123456789"));

        /*set maxLength*/
        binding.editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});

        binding.editText.setText(editTextInput);

        binding.textView.setText(textViewInput);
    }

    public void setEditTextInput(String string) {
        int right = (int) getResources().getDimension(R.dimen.editTextPaddingRight);
        int left = (int) getResources().getDimension(R.dimen.editTextPaddingLeft);
        int top = (int) getResources().getDimension(R.dimen.editTextPaddingTop);
        int bottom = (int) getResources().getDimension(R.dimen.editTextPaddingBottom);
        binding.editText.setText(string);
        binding.editText.setPadding(left, top, right, bottom);
    }

    public void setError(String string) {
        int right = (int) getResources().getDimension(R.dimen.editTextPaddingRight);
        int left = (int) getResources().getDimension(R.dimen.editTextPaddingLeft);
        int top = (int) getResources().getDimension(R.dimen.editTextPaddingTop);
        int bottom = (int) getResources().getDimension(R.dimen.editTextPaddingBottom);
        binding.textView.setText(string);
        binding.textView.setVisibility(View.VISIBLE);

        binding.editText.setBackgroundResource(R.drawable.border_full_error_radius);
        binding.editText.setPadding(left, top, right, bottom);
    }

    public void clearError() {
        int right = (int) getResources().getDimension(R.dimen.editTextPaddingRight);
        int left = (int) getResources().getDimension(R.dimen.editTextPaddingLeft);
        int top = (int) getResources().getDimension(R.dimen.editTextPaddingTop);
        int bottom = (int) getResources().getDimension(R.dimen.editTextPaddingBottom);
        binding.textView.setText("");
        binding.textView.setVisibility(View.INVISIBLE);

        binding.editText.setBackgroundResource(R.drawable.border_full_light_radius);
        binding.editText.setPadding(left, top, right, bottom);
    }

    public AppCompatEditText getEditText() {
        return binding.editText;
    }

    public String getEditTextInput() {
        return binding.editText.getText().toString();
    }

    public void setEnglishNum() {
        FontsOverride.setFontEditText(context, FontsOverride.ENGLISH, binding.editText);
    }
}