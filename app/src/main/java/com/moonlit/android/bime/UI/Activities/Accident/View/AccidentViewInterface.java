package com.moonlit.android.bime.UI.Activities.Accident.View;


import com.moonlit.android.bime.Master.NetworkWarning;
import com.moonlit.android.bime.UI.Activities.Accident.Model.POJO.AddSOSMs;

public interface AccidentViewInterface extends NetworkWarning {
    void sosId(String id, AddSOSMs sosModel);
}
