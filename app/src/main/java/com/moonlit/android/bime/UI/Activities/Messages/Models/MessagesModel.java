package com.moonlit.android.bime.UI.Activities.Messages.Models;

/**
 * Created by hoseinmotlagh on 10/20/17.
 */

public class MessagesModel {

    private String title;
    private boolean isSeen;
    private String date;
    private String desc;

    public MessagesModel(String title, boolean isSeen, String date, String desc) {
        this.title = title;
        this.isSeen = isSeen;
        this.date = date;
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean getIsSeen() {
        return isSeen;
    }

    public void setIsSeen(boolean isSeen) {
        this.isSeen = isSeen;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
