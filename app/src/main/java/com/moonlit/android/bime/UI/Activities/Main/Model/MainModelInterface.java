package com.moonlit.android.bime.UI.Activities.Main.Model;

import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.UI.Activities.Main.Model.POJO.SpeechMS;

interface MainModelInterface {
    void aiSpeechApi(SpeechMS speechMS, ServerListener serverListenerListener);
}
