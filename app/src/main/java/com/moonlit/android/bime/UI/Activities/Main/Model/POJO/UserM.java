package com.moonlit.android.bime.UI.Activities.Main.Model.POJO;

/**
 * Created by p.kokabi on 5/16/2017.
 */

public class UserM {

    private String id, password, newPassword, countryId = "1";
    private String name, family, email, username, code;
    private double credit;
    private boolean isNewPasswordSet;

    public UserM() {
    }

    public UserM(String password, String newPassword) {
        name = "NoN";
        family = "NoN";
        email = "NoN@Non.Non";
        isNewPasswordSet = true;
        this.password = password;
        this.newPassword = newPassword;
    }

    public UserM(String name, String family, String email) {
        this.name = name;
        this.family = family;
        this.email = email;
        isNewPasswordSet = false;
        password = "NoNNoN";
        newPassword = "NoNNoN";
    }

    public UserM(String mobile, String code, String password, boolean isReset) {
        username = mobile;
        this.code = code;
        this.password = password;
    }

    public UserM(String name, String family, String email, String password) {
        this.name = name;
        this.family = family;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFamily() {
        return family;
    }

    public String getFullName() {
        return name + " " + family;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return "0" + username;
    }

    public long getCredit() {
        return (long) credit;
    }

}
