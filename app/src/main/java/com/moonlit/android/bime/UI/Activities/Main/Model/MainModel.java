package com.moonlit.android.bime.UI.Activities.Main.Model;

import com.google.gson.JsonObject;
import com.moonlit.android.bime.Api.ApiClient;
import com.moonlit.android.bime.Api.ApiName.GeneralApiInterface;
import com.moonlit.android.bime.Api.ConstantsMethodServer;
import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.Api.ServerResponse;
import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.Master.ModelBase;
import com.moonlit.android.bime.UI.Activities.Main.Model.POJO.SpeechMS;

public class MainModel extends ModelBase implements MainModelInterface {

    private ServerListener serverListenerListener;

    @Override
    public void onSuccess(IdentifyModel identify, JsonObject data) {
        serverListenerListener.onSuccess(identify, data);
    }

    @Override
    public void onFailure(IdentifyModel identify, String message) {
        serverListenerListener.onFailure(identify, message);
    }

    @Override
    public void aiSpeechApi(SpeechMS speechMS, ServerListener serverListenerListener) {
        this.serverListenerListener = serverListenerListener;
        new ServerResponse()
                .setCall(new IdentifyModel(ConstantsMethodServer.getFirstServerData), ApiClient.getClient()
                        .create(GeneralApiInterface.class)
                        .aiSpeech(speechMS), this);
    }
}
