package com.moonlit.android.bime.Api;

import com.google.gson.JsonObject;

import com.moonlit.android.bime.Master.IdentifyModel;

public interface ServerListener {

    void onSuccess(IdentifyModel identify, JsonObject data);

    void onFailure(IdentifyModel identify, String error);

}