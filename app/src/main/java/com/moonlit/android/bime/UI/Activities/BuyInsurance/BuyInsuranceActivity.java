package com.moonlit.android.bime.UI.Activities.BuyInsurance;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.moonlit.android.bime.Master.CActivity;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.databinding.ActivityBuyInsuranceBinding;

/**
 * Created by hoseinmotlagh on 10/20/17.
 */

public class BuyInsuranceActivity extends CActivity {
    ActivityBuyInsuranceBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialActivity();
        initialDataActivity();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_buy_insurance);
        binding.toolbar.titleTv.setText("خرید بیمه");
        binding.toolbar.plus.setVisibility(View.INVISIBLE);
        binding.toolbar.backImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
