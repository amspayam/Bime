package com.moonlit.android.bime.UI.Activities.Messages;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.Messages.Models.MessagesModel;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.View.AdapterInsuranceListener;
import com.moonlit.android.bime.databinding.ItemMessagesBinding;

import java.util.ArrayList;

import io.rmiri.skeleton.Master.SkeletonConfig;

class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {

    private Context context;
    private ArrayList<MessagesModel> contentList = new ArrayList<>();
    private int size;
    private SkeletonConfig skeletonConfig;
    private AdapterInsuranceListener adapterListener;

    MessagesAdapter(Context context, ArrayList<MessagesModel> dataInput
            , SkeletonConfig skeletonConfig, AdapterInsuranceListener adapterListener) {
        this.context = context;
        contentList = dataInput;
        this.size = size;
        if (skeletonConfig != null)
            this.skeletonConfig = skeletonConfig;
        this.adapterListener = adapterListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemMessagesBinding binding;

        ViewHolder(ItemMessagesBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

//            binding.contentImgv.getLayoutParams().width = size;
//            binding.contentImgv.getLayoutParams().height = size;
//            binding.contentRly.getLayoutParams().height = (int) (size * 0.5625);
        }

        public void bind(final MessagesModel item) {
            binding.bullet.setVisibility(item.getIsSeen() ? View.VISIBLE : View.GONE);
            binding.desc.setText(item.getDesc());
            binding.date.setText(item.getDate());
            binding.title.setText(item.getTitle());
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View view) {
//            adapterListener.onItemClick(contentList.get(getAdapterPosition()).getIdContent()
//                    , contentList.get(getAdapterPosition()).getContentType());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemMessagesBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_messages, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (contentList.size() > 0)
            holder.bind(contentList.get(position));
    }

    @Override
    public int getItemCount() {
        if (skeletonConfig.isSkeletonIsOn())
            return 2;
        else
            return contentList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void addMoreDataAndSkeletonFinish(ArrayList<MessagesModel> dataObjects) {

        //add new data to dataObjects
        contentList = new ArrayList<>();
        contentList.addAll(dataObjects);

        //set false show s
        skeletonConfig.setSkeletonIsOn(false);

        //update items cardView
        notifyDataSetChanged();
    }
}
