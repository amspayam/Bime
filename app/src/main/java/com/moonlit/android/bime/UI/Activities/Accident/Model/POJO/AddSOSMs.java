package com.moonlit.android.bime.UI.Activities.Accident.Model.POJO;

import com.google.gson.annotations.SerializedName;
import com.moonlit.android.bime.Api.ModelServer.RequestBaseM;

/**
 * Created by p.kokabi on 10/20/17.
 */

public class AddSOSMs extends RequestBaseM {

    @SerializedName("IsPoliceNeed")
    private boolean isPoliceNeed;
    @SerializedName("IsAmbulanceNeed")
    private boolean isAmbulanceNeed;
    @SerializedName("IsFireStationsNeed")
    private boolean isFireStationsNeed;
    @SerializedName("IsJurisdictionNeed")
    private boolean isJurisdictionNeed;
    @SerializedName("IsjudgeNeed")
    private boolean isJudgeNeed;
    @SerializedName("Description")
    private String description;
    @SerializedName("X")
    private double lat;
    @SerializedName("Y")
    private double lng;
    @SerializedName("IsOnline")
    private boolean isOnline = true;

    public AddSOSMs(boolean isPoliceNeed, boolean isAmbulanceNeed
            , boolean isFireStationsNeed, boolean isJurisdictionNeed
            , boolean isJudgeNeed, String description, double lat, double lng) {
        this.isPoliceNeed = isPoliceNeed;
        this.isAmbulanceNeed = isAmbulanceNeed;
        this.isFireStationsNeed = isFireStationsNeed;
        this.isJurisdictionNeed = isJurisdictionNeed;
        this.isJudgeNeed = isJudgeNeed;
        this.description = description;
        this.lat = lat;
        this.lng = lng;
    }

    public boolean isPoliceNeed() {
        return isPoliceNeed;
    }

    public boolean isAmbulanceNeed() {
        return isAmbulanceNeed;
    }

    public boolean isFireStationsNeed() {
        return isFireStationsNeed;
    }

    public boolean isJurisdictionNeed() {
        return isJurisdictionNeed;
    }

    public boolean isJudgeNeed() {
        return isJudgeNeed;
    }
}
