package com.moonlit.android.bime.UI.Activities.AccidentConfirm.View;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.moonlit.android.bime.Master.CActivity;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.Accident.Model.POJO.AddSOSMs;
import com.moonlit.android.bime.UI.Activities.AccidentConfirm.Peresent.AccidentConfirmPresenter;
import com.moonlit.android.bime.databinding.ActivityAccidentConfirmBinding;

import ir.pkokabi.alertview.AlertView;
import ir.pkokabi.pdialogs.DialogGeneral.InternetWarning;

public class AccidentConfirmActivity extends CActivity implements AccidentConfirmViewInterface {

    ActivityAccidentConfirmBinding binding;
    AccidentConfirmPresenter accidentPresenter;

    private static String IN_REVIEW = "در حال بررسی";

    private AddSOSMs sosModel;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle bundle = intent.getExtras();
        if (bundle != null)
            sosModel = new Gson().fromJson(bundle.getString("sos", ""), AddSOSMs.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_accident_confirm);

        initialActivity();
        onNewIntent(getIntent());
        initialDataActivity();

    }

    @Override
    public void networkWarning(boolean showRetry) {
        new InternetWarning(context);
    }

    @Override
    public void showError(String message) {
        new AlertView(context, message, AlertView.STATE_ERROR);
    }

    @Override
    public void showMessage(String message) {
        new AlertView(context, message, AlertView.STATE_SUCCESS);
    }

    @Override
    public void onClick(View view) {
        showMessage("در حال حاضر این سرویس فعال نیست");
    }

    @Override
    protected void initialDataActivity() {
        super.initialDataActivity();

        accidentPresenter = new AccidentConfirmPresenter(context, this);
        toolBarConfig(CLOSE, DEFAULT_COLOR, null);
        setListenerForViews();

        binding.dashedLineImgv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        if (sosModel.isPoliceNeed())
            binding.copTv.setText(IN_REVIEW);
        else {
            binding.copBtn.setEnabled(false);
            binding.copBtn.setBackgroundResource(R.drawable.shape_disable_32);
        }

        if (sosModel.isAmbulanceNeed())
            binding.ambulanceTv.setText(IN_REVIEW);
        else {
            binding.ambulanceBtn.setEnabled(false);
            binding.ambulanceBtn.setBackgroundResource(R.drawable.shape_disable_32);
        }

        if (sosModel.isFireStationsNeed())
            binding.fireFighterTv.setText(IN_REVIEW);
        else {
            binding.fireFighterBtn.setEnabled(false);
            binding.fireFighterBtn.setBackgroundResource(R.drawable.shape_disable_32);
        }

        if (sosModel.isJurisdictionNeed())
            binding.craneTv.setText(IN_REVIEW);
        else {
            binding.craneBtn.setEnabled(false);
            binding.craneBtn.setBackgroundResource(R.drawable.shape_disable_32);
        }

        if (sosModel.isJudgeNeed())
            binding.agentTv.setText(IN_REVIEW);
        else {
            binding.agentBtn.setEnabled(false);
            binding.agentBtn.setBackgroundResource(R.drawable.shape_disable_32);
        }
    }

    @Override
    public void setListenerForViews() {
        binding.copBtn.setOnClickListener(this);
        binding.ambulanceBtn.setOnClickListener(this);
        binding.fireFighterBtn.setOnClickListener(this);
        binding.craneBtn.setOnClickListener(this);
        binding.agentBtn.setOnClickListener(this);
    }

}