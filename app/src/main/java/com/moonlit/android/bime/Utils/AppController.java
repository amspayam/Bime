package com.moonlit.android.bime.Utils;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.moonlit.android.bime.Config.SystemConfig;
import com.moonlit.android.bime.Config.UserConfig;
import com.moonlit.android.bime.Utils.Logger.CLog;

import net.gotev.speech.Logger;
import net.gotev.speech.Speech;

public class AppController extends Application {

    private static AppController instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

//        Fabric.with(this, new Crashlytics());

        FirebaseAnalytics.getInstance(getApplicationContext());

        UserConfig.loadUserInfoPreferences(getApplicationContext());
        SystemConfig.loadSystemPreferences(getApplicationContext());

        new FontsOverride(getApplicationContext(), "MONOSPACE", FontsOverride.SANS);

        MultiDex.install(this);
        new CLog();

        //voice
        Speech.init(this, getPackageName());
        Logger.setLogLevel(Logger.LogLevel.DEBUG);
    }

    public static synchronized AppController getInstance() {
        return instance;
    }

}