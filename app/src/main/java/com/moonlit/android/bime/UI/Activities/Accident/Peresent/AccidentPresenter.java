package com.moonlit.android.bime.UI.Activities.Accident.Peresent;


import android.content.Context;

import com.google.gson.JsonObject;
import com.moonlit.android.bime.Api.ConstantsMethodServer;
import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.UI.Activities.Accident.Model.AccidentModel;
import com.moonlit.android.bime.UI.Activities.Accident.Model.POJO.AddSOSMs;
import com.moonlit.android.bime.UI.Activities.Accident.View.AccidentViewInterface;
import com.moonlit.android.bime.Utils.NetworkUtils;

public class AccidentPresenter implements AccidentPresenterInterface, ServerListener {

    private Context context;
    private AccidentViewInterface accidentViewInterface;
    private AccidentModel accidentModel;
    private AddSOSMs sosModel;

    public AccidentPresenter(Context context, AccidentViewInterface accidentViewInterface) {
        this.context = context;
        this.accidentViewInterface = accidentViewInterface;
        accidentModel = new AccidentModel();
    }

    public void addSOS(AddSOSMs sosModel) {
        if (new NetworkUtils(context).isOnline()) {
            accidentModel.addSOSApi(sosModel, this);
            this.sosModel = sosModel;
        } else accidentViewInterface.networkWarning(false);
    }

    @Override
    public void onSuccess(IdentifyModel identify, JsonObject data) {
        switch (identify.getMethodName()) {
            case ConstantsMethodServer.getFirstServerData:
                resultAddSOS(data);
                break;
        }
    }

    @Override
    public void onFailure(IdentifyModel identify, String error) {
        accidentViewInterface.showMessage(error);
    }

    @Override
    public void resultAddSOS(JsonObject data) {
        accidentViewInterface.sosId(data.get("Result").getAsString(), sosModel);
    }

}
