package com.moonlit.android.bime.UI.Activities.Inquiry.Model;

import com.google.gson.JsonObject;
import com.moonlit.android.bime.Api.ApiClient;
import com.moonlit.android.bime.Api.ApiName.GeneralApiInterface;
import com.moonlit.android.bime.Api.ConstantsMethodServer;
import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.Api.ServerResponse;
import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.Master.ModelBase;
import com.moonlit.android.bime.UI.Activities.Inquiry.Model.POJO.InquiryMS;

public class InquiryModel extends ModelBase implements InquiryModelInterface {

    private ServerListener serverListener;

    @Override
    public void onSuccess(IdentifyModel identify, JsonObject data) {
        serverListener.onSuccess(identify, data);
    }

    @Override
    public void onFailure(IdentifyModel identify, String message) {
        serverListener.onFailure(identify, message);
    }

    @Override
    public void estimateApi(InquiryMS inquiryMS, ServerListener serverListener) {
        this.serverListener = serverListener;
        new ServerResponse()
                .setCall(new IdentifyModel(ConstantsMethodServer.getFirstServerData), ApiClient.getClient()
                        .create(GeneralApiInterface.class)
                        .estimatePrice(inquiryMS), this);
    }

}
