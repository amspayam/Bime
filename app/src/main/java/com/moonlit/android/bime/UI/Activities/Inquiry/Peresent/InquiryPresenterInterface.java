package com.moonlit.android.bime.UI.Activities.Inquiry.Peresent;

import com.google.gson.JsonObject;

interface InquiryPresenterInterface {

    void resultEstimate(JsonObject data);

}
