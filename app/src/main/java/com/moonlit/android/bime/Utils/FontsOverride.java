package com.moonlit.android.bime.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.Hashtable;

public class FontsOverride {

    private static Hashtable<String, Typeface> FONT_CACHE = new Hashtable<>();
    /*Fonts*/
    public static String SANS = "fonts/iran_sans.ttf";
    public static String ENGLISH = "fonts/english.ttf";

    FontsOverride(Context context, String staticTypefaceFieldName, String fontAssetName) {
        final Typeface regular = Typeface.createFromAsset(context.getAssets(), fontAssetName);
        replaceFont(staticTypefaceFieldName, regular);
    }

    public static void setFontTextView(Context context, String font, TextView view) {
        Typeface tf = getTypeface(context, font);
        if (tf != null) {
            view.setTypeface(tf);
        }
    }

    public static void setFontEditText(Context context, String font, EditText view) {
        Typeface tf = getTypeface(context, font);
        if (tf != null) {
            view.setTypeface(tf);
        }
    }

    private void replaceFont(String staticTypefaceFieldName, final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class
                    .getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private static Typeface getTypeface(Context context, String font) {
        Typeface tf = FONT_CACHE.get(font);
        if (tf == null) {
            try {
                if (font.equals(SANS))
                    tf = Typeface.createFromAsset(context.getAssets(), SANS);
                else if (font.equals(ENGLISH))
                    tf = Typeface.createFromAsset(context.getAssets(), ENGLISH);
            } catch (Exception e) {
                return null;
            }
            FONT_CACHE.put(font, tf);
        }
        return tf;
    }

}
