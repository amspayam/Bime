package com.moonlit.android.bime.Utils;

import com.moonlit.android.bime.Config.SystemConfig;
import com.moonlit.android.bime.Config.UserConfig;
import com.moonlit.android.bime.R;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.Locale;

public class Converter {

    private String value;
    public final static int FARSI_NUM_CONVERTER = 1;
    public final static int DISTRICT = 2;

    public Converter(String value) {
        Collections.addAll(SystemConfig.SUPPORTED_DISTRICT, value.split("-"));
    }

    public Converter(String toman, boolean hasLabel) {
        if (hasLabel)
            value = String.valueOf(addCommaSeparator(toman)
                    + " " + AppController.getInstance().getString(R.string.toman));
        else
            value = addCommaSeparator(toman);
    }

    public Converter(String input, int type) {
        switch (type) {
            case FARSI_NUM_CONVERTER:
                char character = input.charAt(0);
                int ascii = (int) character;
                if (ascii >= 1632 && ascii <= 1641) {
                    //arabic
                    int valueOld = ascii - 1584;
                    char valueChar = (char) valueOld;
                    value = value + String.valueOf(valueChar);
                } else if (ascii >= 1776 && ascii <= 1785) {
                    //persian
                    int valueOld = ascii - 1728;
                    char valueChar = (char) valueOld;
                    value = value + String.valueOf(valueChar);
                } else {
                    value = input;
                }
                break;
            case DISTRICT:
                char[] chars = new char[input.length()];
                for (int i = 0; i < input.length(); i++) {
                    char ch = input.charAt(i);
                    if (ch >= 0x0660 && ch <= 0x0669)
                        ch -= 0x0660 - '0';
                    else if (ch >= 0x06f0 && ch <= 0x06F9)
                        ch -= 0x06f0 - '0';
                    chars[i] = ch;
                }
                value = new String(chars);
                break;
        }
    }

    public Converter(String idPayment, String redirectPage, String paymentType) {
        value = "http://mobile.ottoshop.ir/api/payment/index?bankType=1&paymentType=" + paymentType +
                "&paymentID=" + idPayment + "&redirectPage=" + redirectPage
                + "&userToken=" + UserConfig.TOKEN;
    }

    public String getValue() {
        return value;
    }

    private String addCommaSeparator(String str) {
        if (str.length() > 0) {
            String str2 = str.replaceAll("[,]", "");
            BigDecimal parsed = new BigDecimal(str2);
            return NumberFormat.getNumberInstance(Locale.US).format(parsed);
        } else {
            return "0";
        }
    }

    private int getMonth(String month) {
        switch (month) {
            case "فروردین":
                return 1;
            case "اردیبهشت":
                return 2;
            case "خرداد":
                return 3;
            case "تیر":
                return 4;
            case "مرداد":
                return 5;
            case "شهریور":
                return 6;
            case "مهر":
                return 7;
            case "آبان":
                return 8;
            case "آذر":
                return 9;
            case "دی":
                return 10;
            case "بهمن":
                return 11;
            case "اسفند":
                return 12;
            default:
                return 0;
        }
    }

}
