package com.moonlit.android.bime.Utils.Logger;


import android.util.Log;

import com.moonlit.android.bime.BuildConfig;

public class CLog {

    private static long startTime;

    public CLog() {

        if (BuildConfig.DEBUG && BuildConfig.IS_CUSTOM_LOG) {
            if (!BuildConfig.IS_SHOW_FUNCTION_NAME && !BuildConfig.IS_SHOW_THREAD_NAME) {
                Logger.init().methodCount(0).hideThreadInfo();
            } else if (!BuildConfig.IS_SHOW_FUNCTION_NAME) {
                Logger.init().methodCount(0);
            } else if (!BuildConfig.IS_SHOW_THREAD_NAME) {
                Logger.init().hideThreadInfo();
            }
        }

    }

    public static void startTimer() {
        if (BuildConfig.DEBUG) {
            if (BuildConfig.IS_CUSTOM_LOG) {
                startTime = System.currentTimeMillis();
                Logger.t("startTimer").i(String.valueOf(startTime));
            } else {
                startTime = System.currentTimeMillis();
                Log.i("====" + "startTimer", String.valueOf(startTime));
            }
        }
    }

    public static void endTimer() {
        if (BuildConfig.DEBUG) {
            long endTime;
            if (BuildConfig.IS_CUSTOM_LOG) {
                endTime = System.currentTimeMillis();
                Logger.t("endTimer").i(String.valueOf(endTime));
                Logger.t("durationTimer").i(String.valueOf(endTime - startTime));
            } else {
                endTime = System.currentTimeMillis();
                Log.i("====" + "endTimer", String.valueOf(endTime));
                Log.i("====" + "durationTimer", String.valueOf(endTime - startTime));
            }
        }
    }

    public static void i(String tag, String message) {
        if (BuildConfig.DEBUG) {
            if (BuildConfig.IS_CUSTOM_LOG) {
                Logger.t(tag).i(message);
            } else {
                Log.i("====" + tag, message);
            }
        }
    }

    public static void e(String tag, String message) {
        if (BuildConfig.DEBUG) {
            if (BuildConfig.IS_CUSTOM_LOG) {
                Logger.t(tag).e(message);
            } else {
                Log.e("====" + tag, message);
            }
        }
    }

    public static void d(String tag, String message) {
        if (BuildConfig.DEBUG) {
            if (BuildConfig.IS_CUSTOM_LOG) {
                Logger.t(tag).d(message);
            } else {
                Log.d("====" + tag, message);
            }
        }
    }

    public static void v(String tag, String message) {
        if (BuildConfig.DEBUG) {
            if (BuildConfig.IS_CUSTOM_LOG) {
                Logger.t(tag).v(message);
            } else {
                Log.v("====" + tag, message);
            }
        }
    }

    public static void json(String tag, String json) {
        if (BuildConfig.DEBUG) {
            if (BuildConfig.IS_CUSTOM_LOG) {
                Logger.t(tag).json(json);
            } else {
                Log.i("====" + tag, json);
            }
        }
    }

}
