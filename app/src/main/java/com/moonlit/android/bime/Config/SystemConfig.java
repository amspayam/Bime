package com.moonlit.android.bime.Config;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import com.moonlit.android.bime.Utils.SharedPreferenceSecret.SharedPreferencesCrypt;

import static android.content.Context.MODE_PRIVATE;

public class SystemConfig {

    private static String SYSTEM_CONFIG_SHARE_PREFERENCES = "SystemConfig";

    public static int INTERVAL_NOTIFICATION = 60 * 1000;
    public static String SUPPORT_PHONE = "";
    public static String SUPPORT_EMAIL = "";
    public static ArrayList<String> SUPPORTED_DISTRICT = new ArrayList<>();
    public static boolean FORCE_UPDATE = false;

    public static void loadSystemPreferences(Context context) {
        SharedPreferences pref = context.getSharedPreferences(SYSTEM_CONFIG_SHARE_PREFERENCES, MODE_PRIVATE);
        SharedPreferencesCrypt sharedPreferencesCrypt = new SharedPreferencesCrypt(pref);
        //==========================================================================================
        INTERVAL_NOTIFICATION = sharedPreferencesCrypt.getInt(GS.INTERVAL_NOTIFICATION, INTERVAL_NOTIFICATION);
        SUPPORT_PHONE = sharedPreferencesCrypt.getString(GS.SUPPORT_PHONE, SUPPORT_PHONE);
        SUPPORT_EMAIL = sharedPreferencesCrypt.getString(GS.SUPPORT_EMAIL, SUPPORT_EMAIL);
        FORCE_UPDATE = sharedPreferencesCrypt.getBoolean(GS.FORCE_UPDATE, FORCE_UPDATE);
    }

    public static void saveSystemPreferences(Context context) {
        SharedPreferences pref = context.getSharedPreferences(SYSTEM_CONFIG_SHARE_PREFERENCES, MODE_PRIVATE);
        SharedPreferencesCrypt sharedPreferencesCrypt = new SharedPreferencesCrypt(pref);
        //==========================================================================================
        sharedPreferencesCrypt.putInt(GS.INTERVAL_NOTIFICATION, INTERVAL_NOTIFICATION);
        sharedPreferencesCrypt.putString(GS.SUPPORT_PHONE, SUPPORT_PHONE);
        sharedPreferencesCrypt.putString(GS.SUPPORT_EMAIL, SUPPORT_EMAIL);
        sharedPreferencesCrypt.putBoolean(GS.FORCE_UPDATE, FORCE_UPDATE);
    }

}