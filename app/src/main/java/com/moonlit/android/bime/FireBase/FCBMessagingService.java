/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.moonlit.android.bime.FireBase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import com.moonlit.android.bime.Config.SystemConfig;
import com.moonlit.android.bime.R;

public class FCBMessagingService extends FirebaseMessagingService {

    public static final String CHANNEL_ID = "ottoshopClients";

    private final static String ORDER_ACCEPTED = "1";
    private final static String DISCOUNT_CODE = "2";
    private final static String SERVICE_STARTED = "3";
    private final static String SERVICE_ENDED = "4";
    private final static String SERVICE_PART_PRICE = "5";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData() != null)
            sendNotificationBackground(remoteMessage.getData());
    }

    private void sendNotificationBackground(Map<String, String> data) {
        if (!SystemConfig.FORCE_UPDATE) {
            String title = data.get("title");
            String body = data.get("body");
            String type = data.get("type");
            Intent intent = null;
            if (intent != null) {
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent,
                        PendingIntent.FLAG_ONE_SHOT);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID
                            , title, NotificationManager.IMPORTANCE_HIGH);
                    notificationChannel.setDescription(body);
                    notificationManager.createNotificationChannel(notificationChannel);

                    Notification.Builder notificationBuilder = new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_stat_name)
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setContentIntent(pendingIntent);
                    notificationManager.notify(0, notificationBuilder.build());
                } else {
                    Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                            + "://" + getPackageName() + "/raw/notification");
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_stat_name)
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(alarmSound)
                            .setContentIntent(pendingIntent);
                    notificationManager.notify(0, notificationBuilder.build());
                }
            }
        }
    }
}
