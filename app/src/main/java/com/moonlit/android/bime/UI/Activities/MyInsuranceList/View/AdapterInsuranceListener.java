package com.moonlit.android.bime.UI.Activities.MyInsuranceList.View;

import com.moonlit.android.bime.UI.Activities.MyInsuranceList.Model.POJO.InsuranceModel;

/**
 * Created by p.kokabi on 9/12/2017.
 */

public interface AdapterInsuranceListener {

    void onItemClick(InsuranceModel insuranceModel, boolean b);

}
