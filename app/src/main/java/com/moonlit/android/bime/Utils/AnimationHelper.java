package com.moonlit.android.bime.Utils;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

/**
 * Created by p.kokabi on 7/11/2017.
 */

public class AnimationHelper {

    public static long ONE_SECOND = 500;
    public static long TWO_SECONDS = 2000;

    public static void fadIn(View view) {
        if (view.getAlpha() != 1) {
            ObjectAnimator animAlphaVisible = ObjectAnimator.ofFloat(view, View.ALPHA, 0.0f, 1.0f);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setInterpolator(new DecelerateInterpolator());
            animatorSet.play(animAlphaVisible);
            animatorSet.setDuration(1000);
            animatorSet.start();
            view.setEnabled(true);
        }
    }

    public static void fadOut(View view) {
        if (view.getAlpha() != 0) {
            ObjectAnimator animAlphaInVisible = ObjectAnimator.ofFloat(view, View.ALPHA, 1.0f, 0.0f);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setInterpolator(new DecelerateInterpolator());
            animatorSet.play(animAlphaInVisible);
            animatorSet.setDuration(1000);
            animatorSet.start();
            view.setEnabled(false);
        }
    }

    public static void translate(View view, float translateTo, long duration) {
        view.animate().setDuration(duration).translationY(translateTo);
    }

}
