package com.moonlit.android.bime.UI.Activities.AccidentConfirm.Peresent;


import android.content.Context;

import com.google.gson.JsonObject;
import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.UI.Activities.AccidentConfirm.Model.AccidentConfirmModel;
import com.moonlit.android.bime.UI.Activities.AccidentConfirm.View.AccidentConfirmViewInterface;

public class AccidentConfirmPresenter implements AccidentConfirmPresenterInterface, ServerListener {

    private Context context;
    private AccidentConfirmViewInterface accidentViewInterface;
    private AccidentConfirmModel accidentConfirmModel;

    public AccidentConfirmPresenter(Context context, AccidentConfirmViewInterface accidentViewInterface) {
        this.context = context;
        this.accidentViewInterface = accidentViewInterface;
        accidentConfirmModel = new AccidentConfirmModel();
    }

    @Override
    public void onSuccess(IdentifyModel identify, JsonObject data) {

    }

    @Override
    public void onFailure(IdentifyModel identify, String error) {

    }

}
