package com.moonlit.android.bime.Api;

import com.moonlit.android.bime.Api.ModelServer.ResponseBaseM;

class ErrorHandling {

    ErrorHandling() {
    }

    String ErrorCode(ResponseBaseM getResponseBaseM) {

        int statusCode = getResponseBaseM.getStatusCode();
        String statusMessage;

        if (statusCode != 200) {
            if (statusCode == 401)
                statusMessage = "401";
            else if (statusCode == 500)
                statusMessage = "مشکل ارتباط با سرور";
            else
                statusMessage = getResponseBaseM.getStatusMessage();
        } else
            statusMessage = "OK";
        return statusMessage;
    }
}
