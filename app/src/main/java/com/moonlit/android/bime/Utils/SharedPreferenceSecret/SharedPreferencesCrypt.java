package com.moonlit.android.bime.Utils.SharedPreferenceSecret;

import android.content.SharedPreferences;

import java.util.Random;

public class SharedPreferencesCrypt {

    private SharePreferenceCipher sharePreferenceCipher = new SharePreferenceCipher();
    private SharedPreferences sharedPreferences;

    public SharedPreferencesCrypt(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    /*============================================================================================*/
    /*String*/

    public String getString(String title, String defaultValue) {
        try {
            return sharePreferenceCipher.decrypt(sharedPreferences.getString(Encode(title), Encode(defaultValue)));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void putString(String title, String value) {
        sharedPreferences.edit().putString(Encode(title), Encode(value)).apply();
    }

    /*============================================================================================*/
    /*Integer*/

    public int getInt(String title, int defaultValue) {
        return ConvertStringToInteger(sharedPreferences.getString(Encode(title), ConvertIntegerToString(defaultValue)));
    }

    public void putInt(String title, int value) {
        sharedPreferences.edit().putString(Encode(title), ConvertIntegerToString(value)).apply();
    }

    private String ConvertIntegerToString(int value) {
        try {
            return sharePreferenceCipher.encrypt(String.valueOf(value));
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }

    private int ConvertStringToInteger(String value) {
        try {
            return Integer.parseInt(sharePreferenceCipher.decrypt(value));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /*============================================================================================*/
    /*Long*/

    public long getLong(String title, long defaultValue) {
        return ConvertStringToLong(sharedPreferences.getString(Encode(title), ConvertLongToString(defaultValue)));
    }

    public void putLong(String title, long value) {
        sharedPreferences.edit().putString(Encode(title), ConvertLongToString(value)).apply();
    }

    private String ConvertLongToString(long value) {
        try {
            return sharePreferenceCipher.encrypt(String.valueOf(value));
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }

    private long ConvertStringToLong(String value) {
        try {
            return Long.parseLong(sharePreferenceCipher.decrypt(value));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /*============================================================================================*/
    /*Boolean*/

    public boolean getBoolean(String title, boolean defaultValue) {
        return ConvertStringToBoolean(sharedPreferences.getString(Encode(title), ConvertBooleanToString(defaultValue)));
    }

    public void putBoolean(String title, boolean value) {
        sharedPreferences.edit().putString(Encode(title), ConvertBooleanToString(value)).apply();
    }

    private String ConvertBooleanToString(boolean value) {
        try {
            //add random number for different boolean (true false) encrypt
            return value ? sharePreferenceCipher.encrypt((String.valueOf(true) + randomNumberGenerate())) : sharePreferenceCipher.encrypt((String.valueOf(false) + randomNumberGenerate()));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private boolean ConvertStringToBoolean(String value) {
        try {
            return Decode(value).subSequence(0, Decode(value).length() - 3).equals(String.valueOf(true));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /*============================================================================================*/
    /* General Crypt String to String */

    private String Decode(String str) {
        try {
            return sharePreferenceCipher.decrypt(str);
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    private String Encode(String str) {
        try {
            return sharePreferenceCipher.encrypt(str);
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    //Random number create, for add in last boolean variable
    private String randomNumberGenerate() {
        return String.valueOf(new Random().nextInt(800 - 240 + 1) + 240);
    }

}