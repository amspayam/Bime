package com.moonlit.android.bime.Api.ModelServer;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class ResponseBaseM {

    @SerializedName("Status")
    private int statusCode;
    @SerializedName("Msg")
    private String statusMessage = null;
    @SerializedName("Data")
    private JsonElement data = null;

    public int getStatusCode() {
        return statusCode;
    }

    public String getStatusMessage() {
        if (statusMessage != null)
            return statusMessage;
        else
            return "";
    }

    public JsonObject getData() {
        if (data != null) {
            JsonObject value = new JsonObject();
            if (!(data instanceof JsonNull))
                value = (JsonObject) data;
            return value;
        } else return new JsonObject();
    }
}

