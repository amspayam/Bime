package com.moonlit.android.bime.UI.Activities.MyInsuranceList.View;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.Model.POJO.InsuranceModel;
import com.moonlit.android.bime.databinding.ItemInsuranceBinding;

import java.util.ArrayList;

import io.rmiri.skeleton.Master.SkeletonConfig;

class MyInsuranceAdapter2 extends RecyclerView.Adapter<MyInsuranceAdapter2.ViewHolder> {

    private Context context;
    private ArrayList<InsuranceModel> contentList = new ArrayList<>();
    private int size;
    private SkeletonConfig skeletonConfig;
    private AdapterInsuranceListener adapterListener;

    MyInsuranceAdapter2(Context context, ArrayList<InsuranceModel> dataInput
            , AdapterInsuranceListener adapterListener) {
        this.context = context;
        contentList = dataInput;
        this.size = size;
        if (skeletonConfig != null)
            this.skeletonConfig = skeletonConfig;
        this.adapterListener = adapterListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemInsuranceBinding binding;

        ViewHolder(ItemInsuranceBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

        }

        public void bind(final InsuranceModel item) {
            binding.titleTv.setText(item.getTitle());
            binding.leftTimeTv.setText(item.getRestDays() + " روز مانده ");
            binding.numberTv.setText(item.getDescription());
            binding.typeTv.setText(" ");
            binding.verticalView.setBackgroundColor(Color.parseColor(item.getColorCode()));
            binding.executePendingBindings();
            binding.notifyChange();

            binding.reminder.setOnClickListener(view -> adapterListener.onItemClick(item, true));
            binding.tamdid.setOnClickListener(view -> adapterListener.onItemClick(item, false));

            binding.executePendingBindings();
        }

        @Override
        public void onClick(View view) {
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemInsuranceBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_insurance, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (contentList.size() > 0)
            holder.bind(contentList.get(position));
    }

    @Override
    public int getItemCount() {
        return contentList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
