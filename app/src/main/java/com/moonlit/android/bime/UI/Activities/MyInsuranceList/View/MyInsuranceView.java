package com.moonlit.android.bime.UI.Activities.MyInsuranceList.View;

import com.moonlit.android.bime.UI.Activities.MyInsuranceList.Model.POJO.InsuranceModel;

import java.util.ArrayList;

/**
 * Created by hoseinmotlagh on 10/19/17.
 */

public interface MyInsuranceView {
    void onInsuranceDataLoad(ArrayList<InsuranceModel> mainModels);
}
