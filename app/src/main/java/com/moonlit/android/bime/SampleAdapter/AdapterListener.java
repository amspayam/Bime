package com.moonlit.android.bime.SampleAdapter;

/**
 * Created by p.kokabi on 9/12/2017.
 */

public interface AdapterListener {

    void onItemClick(String id, short type);

}
