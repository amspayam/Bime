package com.moonlit.android.bime.Config;


public class CarConfig {

    public static final String idOtherServices = "9";
    /*Tire*/
    public static final int idTireInt = 10;
    public static final String idTire = "10";
    public static final String idFrontTire = "14";
    public static final String idRearTire = "15";

    /*WindShield*/
    public static final int idBrakeInt = 11;
    public static final String idBrake = "11";
    public static final String idFrontBrake = "25";
    public static final String idRearBrake = "26";
    public static final String idFrontBrakeWire = "27";
    public static final String idRearBrakeWire = "28";
    public static final String idBrakeDiag = "29";

    /*Oil*/
    public static final int idOilInt = 12;
    public static final String idOil = "12";
    public static final String idOilEngine = "16";
    public static final String idOilFilter = "17";
    public static final String idAirFilter = "18";
    public static final String idCabinFilter = "19";
    public static final String idInjectorCleaner = "21";
    public static final String idEngineCleaner = "23";
    public static final String idOilDiag = "24";
    public static final String idOctan = "22";
    public static final String idWindshieldCleaner = "20";


    /*WindShield*/
    public static final String idWindshield = "8";

    /*Battery*/
    public static final int idBatteryInt = 13;
    public static final String idBattery = "13";
    public static final String id35Amper = "89";
    public static final String id45Amper = "90";
    public static final String id55Amper = "91";
    public static final String id60Amper = "92";
    public static final String id66Amper = "93";
    public static final String id70Amper = "94";
    public static final String id74Amper = "95";
    public static final String id80Amper = "99";
    public static final String id90Amper = "97";
    public static final String id100Amper = "98";
}