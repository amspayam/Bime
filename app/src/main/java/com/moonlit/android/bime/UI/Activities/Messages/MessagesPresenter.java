package com.moonlit.android.bime.UI.Activities.Messages;

import android.content.Context;

import com.google.gson.JsonObject;
import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.UI.Activities.Messages.Models.MessagesModel;

import java.util.ArrayList;

/**
 * Created by hoseinmotlagh on 10/20/17.
 */

public class MessagesPresenter implements ServerListener {


    private Context context;
    private MessagesView messagesView;
    private ArrayList<MessagesModel> messagesModels = new ArrayList<>();

    public MessagesPresenter(Context context, MessagesView messagesView) {

        this.context = context;
        this.messagesView = messagesView;

    }

    public void getMessages() {
        messagesModels.add(new MessagesModel("ارسال کارشناس", true, "۱۵:۲۲ | ۱۲ مردادماه ۹۶", "آقای عباس قادری به شماره ۰۹۳۹۹۰۰۶۵۳۵ به محل شما اعزام شد"));
        messagesModels.add(new MessagesModel("درخواست استعلام قیمت", false, " ۱۲ مردادماه ۹۶", "بیمه بدنه سمند آبی رنگ به شماره پلاک ایران ۹۹ - ۴۹۵ ی ۲۹ به صورت خودکار پرداخت و مبلغ ۴۵,۰۰۰ تومان از اعتبار کاربریتان کسر شد."));
        messagesModels.add(new MessagesModel("تمدید بیمه", false, "۱۲:۰۹ | ۶ تیرماه ۹۶", "برای بیمه بدنه شما تا سقف ۲,۰۰۰,000 هزینه خسارت وارده بر شما را تامین می\u200Cکند"));
        messagesModels.add(new MessagesModel("درخواست استعلام قیمت", true, "۱۲:۰۹ | ۶ تیرماه ۹۶", "برای بیمه بدنه شما تا سقف ۲,۰۰۰,000 هزینه خسارت وارده بر شما را تامین می\u200Cکند"));
        messagesModels.add(new MessagesModel("ارسال کارشناس", false, "۱۵:۲۲ | ۱۲ مردادماه ۹۶", "آقای عباس قادری به شماره ۰۹۳۹۹۰۰۶۵۳۵ به محل شما اعزام شد"));
        messagesModels.add(new MessagesModel("تمدید بیمه", false, "۰۹:۵۴ | ۱۰ تیرماه ۹۶", "بیمه بدنه سمند آبی رنگ به شماره پلاک ایران ۹۹ - ۴۹۵ ی ۲۹ به صورت خودکار پرداخت و مبلغ ۴۵,۰۰۰ تومان از اعتبار کاربریتان کسر شد."));
        messagesView.LoadMessages(messagesModels);
    }

    @Override
    public void onSuccess(IdentifyModel identify, JsonObject data) {

    }

    @Override
    public void onFailure(IdentifyModel identify, String error) {

    }
}
