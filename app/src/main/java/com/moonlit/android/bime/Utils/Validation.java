package com.moonlit.android.bime.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Payam on 8/10/15.
 */
public class Validation {

    private static boolean isMobileValid(String number) {
        if (!number.equals("")) {
            if (number.length() >= 11) {
                if (number.substring(0, 4).contains("091") | number.substring(0, 4).contains("91") | number.substring(0, 4).contains("090")
                        | number.substring(0, 4).contains("90") | number.substring(0, 4).contains("093") | number.substring(0, 4).contains("93")
                        | number.substring(0, 4).contains("092") | number.substring(0, 4).contains("92"))
                    return true;
            } else
                return false;
        }
        return false;
    }

    private static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String email_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(email_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

}