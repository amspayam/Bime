package com.moonlit.android.bime.UI.Activities.AccidentConfirm.Model;

import com.google.gson.JsonObject;
import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.Master.ModelBase;

public class AccidentConfirmModel extends ModelBase implements AccidentConfirmModelInterface {

    private ServerListener serverListenerListener;

    @Override
    public void onSuccess(IdentifyModel identify, JsonObject data) {
        serverListenerListener.onSuccess(identify, data);
    }

    @Override
    public void onFailure(IdentifyModel identify, String message) {
        serverListenerListener.onFailure(identify, message);
    }
}
