package com.moonlit.android.bime.UI.Activities.Main.Model;

/**
 * Created by p.kokabi on 10/20/17.
 */

public class ListModel {

    private String Title;
   private int TypeActivity;


    public ListModel(String title, int typeActivity) {
        Title = title;
        TypeActivity = typeActivity;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getTypeActivity() {
        return TypeActivity;
    }

    public void setTypeActivity(int typeActivity) {
        TypeActivity = typeActivity;
    }
}
