package com.moonlit.android.bime.Components;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.Utils.AnimationHelper;
import com.moonlit.android.bime.Utils.FontsOverride;
import com.moonlit.android.bime.databinding.ViewLoadingBinding;

public class LoadingView extends RelativeLayout implements View.OnClickListener {

    ViewLoadingBinding binding;
    private Context context;

    private LoadingViewListener loadingViewListener; // listener for view

    boolean isFullScreen = true; // If is false view margin top by value actionBarHeight
    int actionBarHeight, statusBarHeight;

    boolean isPauseForSettingActivity = false; // For checking back to activity and call onClickRetry()

    public LoadingView(Context context, boolean isFullScreen, LoadingViewListener loadingViewListener) {
        super(context);
        this.context = context;
        this.isFullScreen = isFullScreen;
        this.loadingViewListener = loadingViewListener;
        init();
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    private void init() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_loading, this, true);

        FontsOverride.setFontTextView(context, FontsOverride.SANS, binding.label);
        FontsOverride.setFontTextView(context, FontsOverride.SANS, binding.labelRetryTv);

        setOnClickListener();
        showLoadingStateView();

        //checking fullScreen
        if (!isFullScreen)
            setMarginTopByValueActionBarHeight();

    }

    private void setOnClickListener() {
        binding.retryLy.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retryLy:
                showLoadingStateView();
                loadingViewListener.onRetryClick();
                break;
        }
    }

    public void showLoadingStateView() {
        binding.loadingView.setVisibility(VISIBLE);
        binding.loadingStateLy.setVisibility(VISIBLE);
        binding.retryLy.setVisibility(GONE);
    }

    public void showRetryStateView() {
        binding.loadingView.setVisibility(VISIBLE);
        binding.loadingStateLy.setVisibility(GONE);
        binding.retryLy.setVisibility(VISIBLE);
    }

    public void dismiss() {
        AnimationHelper.translate(this, 20000, AnimationHelper.TWO_SECONDS);
    }

    public interface LoadingViewListener {
        void onRetryClick();
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (hasWindowFocus) {
            // onResume() called
            if (isPauseForSettingActivity) {
                loadingViewListener.onRetryClick();
                isPauseForSettingActivity = false;
            }
        }
    }

    public void setMarginTopByValueActionBarHeight() {

        // status bar height
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0)
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);

        // Calculate ActionBar height
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());

        // Change margin top
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.setMargins(0, actionBarHeight + statusBarHeight, 0, 0);
        setLayoutParams(params);
    }

    public boolean isFullScreen() {
        return isFullScreen;
    }

    public void setFullScreen(boolean fullScreen) {
        isFullScreen = fullScreen;
    }
}
