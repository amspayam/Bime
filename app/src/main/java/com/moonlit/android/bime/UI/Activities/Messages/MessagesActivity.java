package com.moonlit.android.bime.UI.Activities.Messages;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.moonlit.android.bime.Master.CActivity;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.Messages.Models.MessagesModel;
import com.moonlit.android.bime.databinding.ActivityMessagesBinding;

import java.util.ArrayList;

import io.rmiri.skeleton.Master.SkeletonConfig;

/**
 * Created by hoseinmotlagh on 10/20/17.
 */

public class MessagesActivity extends CActivity implements MessagesView {
    private ActivityMessagesBinding binding;
    private MessagesPresenter messagesPresenter;
    private MessagesAdapter messagesAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_messages);
        initialActivity();
        initialDataActivity();
        messagesPresenter = new MessagesPresenter(getApplicationContext(), this);
        messagesPresenter.getMessages();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.messagesRv.setLayoutManager(linearLayoutManager);
        binding.messagesRv.setHasFixedSize(true);
        binding.toolbar.backImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        binding.toolbar.plus.setVisibility(View.INVISIBLE);
        binding.toolbar.titleTv.setText("پیام ها");
    }

    @Override
    public void LoadMessages(ArrayList<MessagesModel> messagesModels) {
        messagesAdapter = new MessagesAdapter(getApplicationContext(), messagesModels, new SkeletonConfig().build(), null);
        binding.messagesRv.setAdapter(messagesAdapter);
        messagesAdapter.addMoreDataAndSkeletonFinish(messagesModels);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
