package com.moonlit.android.bime.UI.Activities.Dynamic.View;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.moonlit.android.bime.Master.CActivity;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.databinding.ActivityDynamicBinding;

public class DynamicActivity extends CActivity {

    ActivityDynamicBinding binding;

    private String title;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle bundle = intent.getExtras();
        if (bundle != null)
            title = bundle.getString("title", "");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dynamic);

        initialActivity();
        onNewIntent(getIntent());
        initialDataActivity();

    }

    @Override
    protected void initialDataActivity() {
        super.initialDataActivity();

        toolBarConfig(BACK, DEFAULT_COLOR, "خرید بیمه");
        setListenerForViews();

        binding.dynamicTv.setText(title);
    }

}