package com.moonlit.android.bime.SampleAdapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.databinding.ItemContentBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import io.rmiri.skeleton.Master.SkeletonConfig;

class ContentRVAdapter extends RecyclerView.Adapter<ContentRVAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ContentM> contentList = new ArrayList<>();
    private int size;
    private SkeletonConfig skeletonConfig;
    private AdapterListener adapterListener;

    ContentRVAdapter(Context context, ArrayList<ContentM> dataInput
            , SkeletonConfig skeletonConfig, int size, AdapterListener adapterListener) {
        this.context = context;
        contentList = dataInput;
        this.size = size;
        if (skeletonConfig != null)
            this.skeletonConfig = skeletonConfig;
        this.adapterListener = adapterListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemContentBinding binding;

        ViewHolder(ItemContentBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

            binding.contentImgv.getLayoutParams().width = size;
            binding.contentImgv.getLayoutParams().height = size;
            binding.contentRly.getLayoutParams().height = (int) (size * 0.5625);
        }

        public void bind(final ContentM item) {
            if (item.getVideoThumb().length() > 0) {
                Picasso.with(context)
                        .load(item.getVideoThumb())
                        .fit()
                        .centerInside()
                        .into(binding.contentImgv);
            }

            binding.titleTv.setText(item.getServiceName());
            binding.subTitleTv.setText(item.getVideoName());

            binding.getRoot().setOnClickListener(this);

            if (skeletonConfig.isSkeletonIsOn()) {
                binding.skeletonGroup.setAutoPlay(true);
                return;
            } else {
                binding.skeletonGroup.setShowSkeleton(false);
                binding.skeletonGroup.finishAnimation();
            }

            binding.executePendingBindings();
        }

        @Override
        public void onClick(View view) {
            adapterListener.onItemClick(contentList.get(getAdapterPosition()).getIdContent()
                    , contentList.get(getAdapterPosition()).getContentType());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemContentBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_content, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (contentList.size() > 0)
            holder.bind(contentList.get(position));
    }

    @Override
    public int getItemCount() {
        if (skeletonConfig.isSkeletonIsOn())
            return 10;
        else
            return contentList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void addMoreDataAndSkeletonFinish(ArrayList<ContentM> dataObjects) {

        //add new data to dataObjects
        contentList = new ArrayList<>();
        contentList.addAll(dataObjects);

        //set false show s
        skeletonConfig.setSkeletonIsOn(false);

        //update items cardView
        notifyDataSetChanged();
    }
}
