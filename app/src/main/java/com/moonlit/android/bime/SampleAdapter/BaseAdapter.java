package com.moonlit.android.bime.SampleAdapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.rmiri.skeleton.Master.SkeletonConfig;


/**
 * Created by hosein on 5/17/2017.
 */

public abstract class BaseAdapter<T, B extends ViewDataBinding> extends RecyclerView.Adapter<BaseAdapter.ViewHolder> {
    public B binding;
    Context context;
    @Nullable
    private SkeletonConfig skeletonConfig;
    private ArrayList<T> list;

    public BaseAdapter(ArrayList<T> list, Context context, @Nullable SkeletonConfig skeletonConfig) {
        this.list = list;
        this.context = context;
        this.skeletonConfig = skeletonConfig;
    }

    public static class ViewHolder<V extends ViewDataBinding> extends RecyclerView.ViewHolder {
        private V v;

        public ViewHolder(V v) {
            super(v.getRoot());
            this.v = v;
        }

        public V getBinding() {
            return v;
        }
    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return list.get(position).layoutId();
//    }

    @Override
    public BaseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(inflater, getLayoutResourceId(), parent, false);

        binding.executePendingBindings();
        binding.notifyChange();
//        ViewDataBinding bind = DataBindingUtil.bind(LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false));
//         return new ViewHolder<>(bind);

        return new ViewHolder<>(binding);
    }


    protected abstract int getLayoutResourceId();

    protected abstract void setViews(int position, List<? extends T> list, SkeletonConfig skeletonConfig);

    @Override
    public void onBindViewHolder(BaseAdapter.ViewHolder holder, int position) {
//        final T model = list.get(position);
//        holder.getBinding().setVariable(BR.model, model);
//        holder.getBinding().executePendingBindings();

        setViews(position, list, skeletonConfig);// This line is important, it will force to load the variable in a custom view
        binding.executePendingBindings();
        binding.notifyChange();



    }

    @Override
    public int getItemCount() {
//        if (skeletonConfig.isSkeletonIsOn()) {
//            // show just 2 card item in recyclerView
//            return 2;
//        } else {
        //normal show card item in recyclerView
        return list.size();
//        }
    }

    public void addMoreDataAndSkeletonFinish(ArrayList<? extends T> list) {

        //add new data to dataObjects
        this.list = new ArrayList<>();
        this.list.addAll(list);

        //set false show s
        skeletonConfig.setSkeletonIsOn(false);

        //update items cardView
        notifyDataSetChanged();
        binding.notifyChange();
        binding.executePendingBindings();
    }

}