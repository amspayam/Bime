package com.moonlit.android.bime.Components;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by P.kokabi on 7/13/2016.
 */

abstract class EndlessRecyclerList extends RecyclerView.OnScrollListener {
    public static String TAG = EndlessRecyclerList.class.getSimpleName();

    private static int previousTotal = 0; // The total number of items in the dataset after the last load
    private static boolean loading = true; // True if we are still waiting for the last set of data to load.

    private static int current_page = 1;

    private LinearLayoutManager mLinearLayoutManager;

    private static final int HIDE_THRESHOLD = 150;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;

    EndlessRecyclerList(LinearLayoutManager linearLayoutManager) {
        mLinearLayoutManager = linearLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = mLinearLayoutManager.getItemCount();
        int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        /*Pagination Conditions*/
        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        int visibleThreshold = 3;
        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {
            // End has been reached
            // Do something
            current_page++;

            onLoadMore(current_page);

            loading = true;
        }

        /*Show And Hide View Threshold and Conditions*/
        if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
            onHide(scrolledDistance += dy);
            controlsVisible = false;
            scrolledDistance = 0;
        } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
            onShow(scrolledDistance -= dy);
            controlsVisible = true;
            scrolledDistance = 0;
        }

        if ((controlsVisible && dy > 0) || (!controlsVisible && dy < 0)) {
            scrolledDistance += dy;
        }

    }

    static void resetPreviousTotal() {
        previousTotal = 10;
        current_page = 1;
        loading = false;
    }

    public abstract void onLoadMore(int current_page);

    public abstract void onHide(int scrolledDistance);

    public abstract void onShow(int scrolledDistance);
}