package com.moonlit.android.bime.UI.Activities.Messages;

import com.moonlit.android.bime.UI.Activities.Messages.Models.MessagesModel;

import java.util.ArrayList;

/**
 * Created by hoseinmotlagh on 10/20/17.
 */

public interface MessagesView {
    void LoadMessages(ArrayList<MessagesModel> messagesModels);
}
