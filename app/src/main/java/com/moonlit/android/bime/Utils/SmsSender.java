package com.moonlit.android.bime.Utils;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.SmsManager;

/**
 * Created by p.kokabi on 12/13/2016.
 */

public class SmsSender {

    private Context context;

    private BroadcastReceiver regRecSend = null;
    private BroadcastReceiver regRecDeliver = null;
    private String SENT = "SMS_SENT", DELIVERED = "SMS_DELIVERED";

    public void sendSMS(Context context, String message) {
        this.context = context;
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);

        initSendReceiver();
        initDeliverReceiver();
        register();

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage("111111", null, message, sentPI, deliveredPI);


        unregister();
    }

    private void register() {
        context.registerReceiver(regRecSend, new IntentFilter(SENT));
        context.registerReceiver(regRecDeliver, new IntentFilter(DELIVERED));
    }

    private void unregister() {
        if (regRecSend != null) {
            context.unregisterReceiver(regRecSend);
            regRecSend = null;
        }
        if (regRecDeliver != null) {
            context.unregisterReceiver(regRecDeliver);
            regRecDeliver = null;
        }
    }

    private void initSendReceiver() {
        regRecSend = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
            }
        };

    }

    private void initDeliverReceiver() {
        //---when the SMS has been delivered---
        regRecDeliver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
            }
        };
    }

    public void deleteSMS(Context context, String message, String number) {
        try {
            Uri uriSms = Uri.parse("content://sms/inbox");
            Cursor cursor = context.getContentResolver().query(uriSms,
                    new String[]{"_id", "thread_id", "address",
                            "person", "date", "body"}, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    long id = cursor.getLong(0);
                    String address = cursor.getString(2);
                    String body = cursor.getString(5);

                    if (message.equals(body) && address.equals(number)) {
                        context.getContentResolver().delete(Uri.parse("content://sms/" + id), null, null);
                    }
                } while (cursor.moveToNext());
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}