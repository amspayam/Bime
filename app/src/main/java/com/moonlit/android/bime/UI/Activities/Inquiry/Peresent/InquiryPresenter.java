package com.moonlit.android.bime.UI.Activities.Inquiry.Peresent;


import android.content.Context;

import com.google.gson.JsonObject;
import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.Inquiry.Model.InquiryModel;
import com.moonlit.android.bime.UI.Activities.Inquiry.Model.POJO.InquiryMS;
import com.moonlit.android.bime.UI.Activities.Inquiry.View.InquiryInterface;
import com.moonlit.android.bime.Utils.NetworkUtils;

public class InquiryPresenter implements InquiryPresenterInterface, ServerListener {

    private Context context;
    private InquiryInterface inquiryInterface;
    private InquiryModel inquiryModel;

    public InquiryPresenter(Context context, InquiryInterface accidentViewInterface) {
        this.context = context;
        this.inquiryInterface = accidentViewInterface;
        inquiryModel = new InquiryModel();
    }

    public void estimatePrice(InquiryMS inquiryMS) {
        if (new NetworkUtils(context).isOnline())
            inquiryModel.estimateApi(inquiryMS, this);
        else inquiryInterface.networkWarning(false);
    }

    @Override
    public void onSuccess(IdentifyModel identify, JsonObject data) {
        resultEstimate(data);
    }

    @Override
    public void onFailure(IdentifyModel identify, String error) {
        inquiryInterface.showMessage(error);
    }

    @Override
    public void resultEstimate(JsonObject data) {
        if (data.get("Result").getAsBoolean())
            inquiryInterface.showMessage("استعلام قیمت با موقیت ثبت شد ، منتظر پاسخ ما باشید.");
        else inquiryInterface.showError(context.getString(R.string.tryAgain));
    }
}
