package com.moonlit.android.bime.Api;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.moonlit.android.bime.Config.UserConfig;
import com.moonlit.android.bime.Utils.Constants;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

public class ApiClientMedia {

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {

            //Logs request and response lines and their respective headers and bodies
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request.Builder ongoing = chain.request().newBuilder();
                            ongoing.addHeader("content-type"
                                    , "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
                            ongoing.addHeader("cache-control", "no-cache");
                            if (!UserConfig.TOKEN.equals(""))
                                ongoing.addHeader("Token", UserConfig.TOKEN);
                            else
                                ongoing.addHeader("Token", "No-Token");
                            return chain.proceed(ongoing.build());
                        }
                    })
                    .connectTimeout(Constants.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(Constants.READ_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(Constants.WRITE_TIMEOUT, TimeUnit.SECONDS)
                    .addInterceptor(logging)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.URL_API)
                    .client(httpClient)
                    .build();
        }
        return retrofit;
    }
}
