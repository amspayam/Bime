package com.moonlit.android.bime.Api;

import com.google.gson.JsonObject;

import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.Utils.AppController;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ServerResponseMedia {

    public void setCall(final IdentifyModel identify, Call<ResponseBody> call, final ServerListener serverListener) {

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        if (response.message().equals("OK"))
                            serverListener.onSuccess(identify, new JsonObject());
                        else
                            serverListener.onFailure(identify, response.message());
                    } else {
                        serverListener.onFailure(identify, response.message());
                    }
                } catch (Exception e) {
                    serverListener.onFailure(identify, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                serverListener.onFailure(identify, AppController.getInstance().getString(R.string.serverError));
            }

        });
    }

}