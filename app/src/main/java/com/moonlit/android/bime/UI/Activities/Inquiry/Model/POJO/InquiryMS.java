package com.moonlit.android.bime.UI.Activities.Inquiry.Model.POJO;

import com.google.gson.annotations.SerializedName;
import com.moonlit.android.bime.Api.ModelServer.RequestBaseM;

/**
 * Created by p.kokabi on 10/20/17.
 */

public class InquiryMS extends RequestBaseM {

    @SerializedName("BimeNumber")
    private String insuranceNumber;
    @SerializedName("Description")
    private String desc;

    public InquiryMS(String insuranceNumber, String desc) {
        this.insuranceNumber = insuranceNumber;
        this.desc = desc;
    }
}
