package com.moonlit.android.bime.Master;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.moonlit.android.bime.Components.CustomEditText;
import com.moonlit.android.bime.Components.LoadingView;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.Utils.DeviceScreenUtil;
import com.wang.avi.AVLoadingIndicatorView;

public class CActivity extends AppCompatActivity implements View.OnClickListener, LoadingView.LoadingViewListener {

    protected Context context;
    protected LoadingView loadingView;
    protected AVLoadingIndicatorView progressView;
    public FirebaseAnalytics firebaseAnalytics;
    /*ToolBar Views*/
    public Toolbar toolBar;
    public TextView titleTv;
    public AppCompatImageButton backImgBtn, closeImgBtn, confirmImgBtn;
    /*ToolBar*/
    public final static int DEFAULT_TOOLBAR = 0;
    public final static int BACK = 1;
    public final static int CLOSE = 2;
    public final static int CLOSE_BROADCAST = 3;
    public final static int CLOSE_CONFIRM = 4;
    public final static int DEFAULT_COLOR = R.color.darkPrimaryColor;

    private boolean hasBroadCast = false;
    private BroadcastReceiver receiver;
    private String broadCastName = "", secondBroadCastName = "";
    public int height, width;

    @Override
    protected void onResume() {
        context = this;
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (hasBroadCast)
            unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public void onRetryClick() {

    }

    @Override
    public void onClick(View view) {
    }

    public void initialActivity() {
        overridePendingTransition(R.anim.enter_right, R.anim.exit_right);

        context = this;
        titleTv = (TextView) findViewById(R.id.titleTv);

        firebaseAnalytics = FirebaseAnalytics.getInstance(context);

        height = new DeviceScreenUtil(context).getHeight();
        width = new DeviceScreenUtil(context).getWidth();

    }

    public void toolBarConfig(int typeToolbar, int statueColor, @Nullable String title) {

        if (title != null)
            titleTv.setText(title);
        else if (titleTv != null) {
            titleTv.setText("");
        }

        switch (typeToolbar) {
            case BACK:
                backImgBtn = (AppCompatImageButton) findViewById(R.id.backImgBtn);
                backImgBtn.setOnClickListener(v -> finish());
                break;
            case CLOSE:
                closeImgBtn = (AppCompatImageButton) findViewById(R.id.closeImgBtn);
                closeImgBtn.setOnClickListener(v -> finish());
                break;
            case CLOSE_BROADCAST:
                closeImgBtn = (AppCompatImageButton) findViewById(R.id.closeImgBtn);
                closeImgBtn.setOnClickListener(v -> finish());
                break;
        }

        if (statueColor != DEFAULT_COLOR) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(statueColor);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(ContextCompat.getColor(context, DEFAULT_COLOR));
            }
        }

    }

    public void initBroadCast(@Nullable String broadCastName, @Nullable String secondBroadCastName) {
        hasBroadCast = true;
        this.broadCastName = broadCastName;
        this.secondBroadCastName = secondBroadCastName;
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };

        IntentFilter filter = new IntentFilter();
        if (broadCastName == null && secondBroadCastName != null)
            filter.addAction(secondBroadCastName);
        else if (secondBroadCastName == null && broadCastName != null)
            filter.addAction(broadCastName);
        else if (secondBroadCastName != null) {
            filter.addAction(broadCastName);
            filter.addAction(secondBroadCastName);
        }
        registerReceiver(receiver, filter);
    }

    public void initialPassword(CustomEditText password) {
        password.getEditText().setImeOptions(EditorInfo.IME_ACTION_GO);
        password.getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    protected void initialLoadingView(boolean isFullScreen, LoadingView.LoadingViewListener loadingViewListener) {
        loadingView = new LoadingView(getApplicationContext(), isFullScreen, loadingViewListener);
        ViewGroup content = this.getWindow().getDecorView().findViewById(android.R.id.content);
        content.addView(loadingView);
    }

    protected void initialDataActivity() {
    }

    public void setListenerForViews() {
    }

    public void hideStatuesBar() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void startLoading() {
        progressView = (AVLoadingIndicatorView) findViewById(R.id.progressView);
        progressView.smoothToShow();
    }

    public void stopLoading() {
        progressView = (AVLoadingIndicatorView) findViewById(R.id.progressView);
        progressView.hide();
    }

}
