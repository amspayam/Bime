package com.moonlit.android.bime.UI.Activities.MyInsuranceList.View;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.moonlit.android.bime.Master.CActivity;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.SampleAdapter.ContentM;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.AutoPaidDialog;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.Model.POJO.InsuranceModel;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.MyInsurancePresenter;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.ReminderFragment;
import com.moonlit.android.bime.databinding.ActivityMyInsuranceBinding;

import java.util.ArrayList;

public class MyInsuranceActivity extends CActivity implements MyInsuranceView, AdapterInsuranceListener {
    ArrayList<InsuranceModel> mainModels;
    MyInsurancePresenter myInsurancePresenter;

    private ActivityMyInsuranceBinding binding;
    private ArrayList<ContentM> dataObjects = new ArrayList<>();
    private BottomSheetBehavior behavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_insurance);

        initialActivity();
        initialDataActivity();

        binding.toolbar.backImgBtn.setOnClickListener(view -> onBackPressed());

//        binding.insuranceRv.

//        CropImage.activity()
//                .setGuidelines(CropImageView.Guidelines.OFF)
//                .start(this);

        myInsurancePresenter = new MyInsurancePresenter(getApplicationContext(), this);
        mainModels = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.insuranceRv.setLayoutManager(linearLayoutManager);
        binding.insuranceRv.setHasFixedSize(true);
        binding.toolbar.titleTv.setText("بیمه های من");
        myInsurancePresenter.getAllBime();
        behavior = BottomSheetBehavior.from(binding.reminderRl);

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");

                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");

                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        binding.reminderFl.setVisibility(View.GONE);
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");

                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        binding.reminderFl.setVisibility(View.GONE);
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.i("BottomSheetCallback", "called");
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }
    }


    @Override
    public void setListenerForViews() {

    }

    @Override
    public void onInsuranceDataLoad(ArrayList<InsuranceModel> mainModels) {
        binding.insuranceRv.setAdapter(new MyInsuranceAdapter2(getApplicationContext()
                , mainModels, this));

        new Handler().postDelayed(() -> {
            binding.skeletonGroup.setShowSkeleton(false);
            binding.skeletonGroup.finishAnimation();
        }, 2000);
    }

    @Override
    public void onItemClick(InsuranceModel insuranceModel, boolean b) {
        if (b) {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            binding.reminderFl.setVisibility(View.VISIBLE);
            AddFragmentToButtomShit(insuranceModel);
        } else {
            AddFragmentDialogFl(insuranceModel);
        }


    }

    public void closeShit() {
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    public void cloasedialog() {
        binding.dialogfl.setVisibility(View.GONE);
        binding.dialogLayFl.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {

        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (binding.dialogLayFl.getVisibility() == View.VISIBLE) {
            binding.dialogLayFl.setVisibility(View.GONE);
        } else
            super.onBackPressed();
    }

    public void AddFragmentToButtomShit(InsuranceModel insuranceModel) {
        ReminderFragment reminderFragment = ReminderFragment.newInstance(insuranceModel);
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transation = fragmentManager.beginTransaction();
        transation.replace(R.id.reminder_rl, reminderFragment, "reminderFragment");
        transation.commit();
        fragmentManager.executePendingTransactions();
    }

    public void AddFragmentDialogFl(InsuranceModel insuranceModel) {
        binding.dialogLayFl.setVisibility(View.VISIBLE);
        binding.dialogfl.setVisibility(View.VISIBLE);

        AutoPaidDialog dialog = AutoPaidDialog.newInstance(insuranceModel);
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transation = fragmentManager.beginTransaction();
        transation.replace(R.id.dialogfl, dialog, "dialogfl");
        transation.commit();
        fragmentManager.executePendingTransactions();
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                Uri resultUri = result.getUri();
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Exception error = result.getError();
//            }
//        }
//    }
}
