package com.moonlit.android.bime.UI.Activities.Main.View;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.Accident.View.AccidentActivity;
import com.moonlit.android.bime.UI.Activities.BuyInsurance.BuyInsuranceActivity;
import com.moonlit.android.bime.UI.Activities.Dynamic.View.DynamicActivity;
import com.moonlit.android.bime.UI.Activities.Main.Model.ListModel;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.View.AdapterInsuranceListener;
import com.moonlit.android.bime.databinding.ItemLastSeenBinding;

import java.util.ArrayList;

import io.rmiri.skeleton.Master.SkeletonConfig;
import ir.pkokabi.alertview.AlertView;

class LastSeenAdapter2 extends RecyclerView.Adapter<LastSeenAdapter2.ViewHolder> {

    private Context context;
    private ArrayList<ListModel> contentList = new ArrayList<>();
    private int size;
    private SkeletonConfig skeletonConfig;
    private AdapterInsuranceListener adapterListener;

    LastSeenAdapter2(Context context, ArrayList<ListModel> dataInput
            , SkeletonConfig skeletonConfig) {
        this.context = context;
        contentList = dataInput;
        this.size = size;
        if (skeletonConfig != null)
            this.skeletonConfig = skeletonConfig;
        this.adapterListener = adapterListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemLastSeenBinding binding;

        ViewHolder(ItemLastSeenBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

//            binding.contentImgv.getLayoutParams().width = size;
//            binding.contentImgv.getLayoutParams().height = size;
//            binding.contentRly.getLayoutParams().height = (int) (size * 0.5625);
        }

        public void bind(final ListModel item) {

            if (skeletonConfig.isSkeletonIsOn()) {
                //need show s for 5 cards
                binding.skeletonGroup.setAutoPlay(true);
                return;
            } else {
                binding.skeletonGroup.setShowSkeleton(false);
                binding.skeletonGroup.finishAnimation();
            }


            binding.titleTv.setText(item.getTitle());

            binding.titleTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "" + item, Toast.LENGTH_SHORT).show();
                }
            });


            binding.titleTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (item.getTypeActivity()) {
                        case 468662:
                            //kharide bime badane
                            context.startActivity(new Intent(context, DynamicActivity.class)
                                    .putExtra("title", "خرید بیمه بدنه"));
                            break;
                        case 617821:
                        case 503939:
                            //kharide bime omr
                            context.startActivity(new Intent(context, DynamicActivity.class)
                                    .putExtra("title", "خرید بیمه عمر"));
                            break;
                        case 90068:
                            //kharide bime mosaferati
                            context.startActivity(new Intent(context, DynamicActivity.class)
                                    .putExtra("title", "خرید بیمه مسافرتی"));
                            break;
                        case 633806:
                            //sos
                            context.startActivity(new Intent(context, AccidentActivity.class));
                            break;
                        case 986588:
                            //kharide bime atashsozi
                            context.startActivity(new Intent(context, DynamicActivity.class)
                                    .putExtra("title", "خرید بیمه آتش سوزی"));
                            break;
                        case 83638:
                            //kharide bime
                            context.startActivity(new Intent(context, BuyInsuranceActivity.class));
                            break;
                        case 306934:
                            //kharide bime masoliayte pezeshki
                            context.startActivity(new Intent(context, DynamicActivity.class)
                                    .putExtra("title", "خرید بیمه مسولیت پزشکی"));
                            break;
                        case 4545:
                            //kharide bime masoliayte pezeshki
                            context.startActivity(new Intent(context, DynamicActivity.class)
                                    .putExtra("title", "خرید بیمه آرمان"));
                            break;
                        default:
                            new AlertView(context, "پاسخی برای سوال شما در سیستم پیدا نشد، در اسرع وقت کارشناسان ما از طریق نرم افزار پاسخ شما را می دهند"
                                    , AlertView.STATE_WARNING);
                    }

                }
            });

            binding.executePendingBindings();
        }

        @Override
        public void onClick(View view) {
//            adapterListener.onItemClick(contentList.get(getAdapterPosition()).getIdContent()
//                    , contentList.get(getAdapterPosition()).getContentType());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemLastSeenBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_last_seen, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (contentList.size() > 0)
            holder.bind(contentList.get(position));
    }

    @Override
    public int getItemCount() {
        if (skeletonConfig.isSkeletonIsOn())
            return 5;
        else
            return contentList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void addMoreDataAndSkeletonFinish(ArrayList<ListModel> dataObjects) {

        //add new data to dataObjects
        contentList = new ArrayList<>();
        contentList.addAll(dataObjects);

        //set false show s
        skeletonConfig.setSkeletonIsOn(false);

        //update items cardView
        notifyDataSetChanged();
    }
}
