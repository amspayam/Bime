package com.moonlit.android.bime.Master;

/**
 * Created by p.kokabi on 7/19/2017.
 */

public interface NetworkWarning {

    void networkWarning(boolean showRetry);

    void showError(String error);

    void showMessage(String message);

}
