package com.moonlit.android.bime.UI.Activities.Inquiry.Model;

import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.UI.Activities.Inquiry.Model.POJO.InquiryMS;

interface InquiryModelInterface {

    void estimateApi(InquiryMS inquiryMS, ServerListener serverListener);

}
