package com.moonlit.android.bime.Components;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.moonlit.android.bime.Utils.FontsOverride;

public class CTextViewEnglish extends AppCompatTextView {
    Context ctx;

    public CTextViewEnglish(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ctx = context;
        init();
    }

    public CTextViewEnglish(Context context, AttributeSet attrs) {
        super(context, attrs);
        ctx = context;
        init();
    }

    public CTextViewEnglish(Context context) {
        super(context);
        ctx = context;
        init();
    }

    public void init() {
        if (isInEditMode())
            return;
        setOnTouchListener(null);
        FontsOverride.setFontTextView(ctx, FontsOverride.ENGLISH, this);
    }
}