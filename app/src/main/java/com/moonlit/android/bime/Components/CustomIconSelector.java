package com.moonlit.android.bime.Components;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.databinding.CustomIconSelectorBinding;

/**
 * Created by p.kokabi on 9/19/2017.
 */

public class CustomIconSelector extends RelativeLayout implements View.OnClickListener {

    private CustomIconSelectorBinding binding;
    Context context;
    private String title;
    private int defaultIcon;
    private boolean isSelected;

    public CustomIconSelector(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CustomIconSelector(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    public CustomIconSelector(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.generalAttrs);

        title = typedArray.getString(R.styleable.generalAttrs_text);
        defaultIcon = typedArray.getResourceId(R.styleable.generalAttrs_normalIcon, 0);

        init();
        typedArray.recycle();

    }

    public void init() {
        if (isInEditMode())
            return;
        binding = DataBindingUtil
                .inflate(LayoutInflater.from(getContext()), R.layout.custom_icon_selector, this, true);

        binding.iconTileTv.setText(title);
        binding.iconImgv.setImageResource(defaultIcon);
        setOnClickListener(this);

    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    public String isSelectedString() {
        if (isSelected)
            return "1";
        else
            return "0";
    }

    public void select() {
        if (!isSelected) {
            binding.iconImgv.setColorFilter(ContextCompat.getColor(context, R.color.accentColor));
            binding.iconTileTv.setTextColor(ContextCompat.getColor(context, R.color.accentColor));
            binding.selectedImgv.setVisibility(VISIBLE);
            isSelected = true;
        } else {
            binding.iconImgv.setColorFilter(ContextCompat.getColor(context, R.color.black));
            binding.iconTileTv.setTextColor(ContextCompat.getColor(context, R.color.black));
            binding.selectedImgv.setVisibility(GONE);
            isSelected = false;
        }
    }

    @Override
    public void onClick(View v) {
        select();
    }
}
