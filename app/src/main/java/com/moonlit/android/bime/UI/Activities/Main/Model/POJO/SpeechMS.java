package com.moonlit.android.bime.UI.Activities.Main.Model.POJO;

import com.google.gson.annotations.SerializedName;
import com.moonlit.android.bime.Api.ModelServer.RequestBaseM;

/**
 * Created by p.kokabi on 10/20/17.
 */

public class SpeechMS extends RequestBaseM {

    @SerializedName("Text")
    private String text;

    public SpeechMS(String text) {
        this.text = text;
    }
}
