package com.moonlit.android.bime.Config;

import android.content.Context;
import android.content.SharedPreferences;

import com.moonlit.android.bime.Utils.Converter;
import com.moonlit.android.bime.Utils.SharedPreferenceSecret.SharedPreferencesCrypt;

import static android.content.Context.MODE_PRIVATE;

public class UserConfig {

    private static String USER_CONFIG_SHARE_PREFERENCES = "UserConfig";

    public static String TOKEN = "";
    public static String GCM_TOKEN = "";
    public static String MOBILE = "";
    public static String FULL_NAME = "";
    public static String EMAIL = "";
    public static long CREDIT = 0;

    public static void loadUserInfoPreferences(Context context) {
        SharedPreferences pref = context.getSharedPreferences(USER_CONFIG_SHARE_PREFERENCES, MODE_PRIVATE);
        SharedPreferencesCrypt sharedPreferencesCrypt = new SharedPreferencesCrypt(pref);
        //==========================================================================================
        TOKEN = sharedPreferencesCrypt.getString(GS.TOKEN, TOKEN);
        GCM_TOKEN = sharedPreferencesCrypt.getString(GS.GCM_TOKEN, GCM_TOKEN);
        MOBILE = sharedPreferencesCrypt.getString(GS.MOBILE, MOBILE);
        FULL_NAME = sharedPreferencesCrypt.getString(GS.FULL_NAME, FULL_NAME);
        EMAIL = sharedPreferencesCrypt.getString(GS.EMAIL, EMAIL);
        CREDIT = sharedPreferencesCrypt.getLong(GS.CREDIT, CREDIT);
    }

    public static void saveUserInfoPreferences(Context context) {
        SharedPreferences pref = context.getSharedPreferences(USER_CONFIG_SHARE_PREFERENCES, MODE_PRIVATE);
        SharedPreferencesCrypt sharedPreferencesCrypt = new SharedPreferencesCrypt(pref);
        //==========================================================================================
        sharedPreferencesCrypt.putString(GS.TOKEN, TOKEN);
        sharedPreferencesCrypt.putString(GS.GCM_TOKEN, GCM_TOKEN);
        sharedPreferencesCrypt.putString(GS.MOBILE, MOBILE);
        sharedPreferencesCrypt.putString(GS.FULL_NAME, FULL_NAME);
        sharedPreferencesCrypt.putString(GS.EMAIL, EMAIL);
        sharedPreferencesCrypt.putLong(GS.CREDIT, CREDIT);
    }

    public static void signOut(Context context) {
        TOKEN = "";
        MOBILE = "";
        FULL_NAME = "";
        SharedPreferences pref = context.getSharedPreferences(USER_CONFIG_SHARE_PREFERENCES, MODE_PRIVATE);
        pref.edit().clear().apply();
    }

    public static long getCreditInt() {
        return CREDIT;
    }

    public static String getCreditString() {
        return new Converter(String.valueOf(CREDIT), true).getValue();
    }

    public static void setCreditInt(long creditLong) {
        CREDIT = creditLong;
    }
}