package com.moonlit.android.bime.Utils.Logger;

public enum LogLevel {

  /**
   * Prints all logs
   */
  FULL,

  /**
   * No log will be printed
   */
  NONE
}
