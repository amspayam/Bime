package com.moonlit.android.bime.UI.Activities.MyInsuranceList;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.Model.POJO.InsuranceModel;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.View.MyInsuranceActivity;
import com.moonlit.android.bime.databinding.ReminderFragmnetBinding;


/**
 * Created by hosein on 6/25/17.
 */

public class ReminderFragment extends Fragment {
    private ReminderFragmnetBinding binding;
    private int item = 0;


    public static ReminderFragment newInstance(InsuranceModel insuranceModel) {

        Bundle args = new Bundle();
        args.putSerializable("insuranceModel", insuranceModel);
        ReminderFragment fragment = new ReminderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.reminder_fragmnet, container, false);
        View view = binding.getRoot();
        InsuranceModel item = (InsuranceModel) getArguments().getSerializable("insuranceModel");
        binding.titleTv.setText(item.getTitle());
        binding.leftTimeTv.setText(item.getRestDays() + " روز مانده ");
        binding.numberTv.setText(item.getDescription());
        binding.typeTv.setText("");
        Log.e("insuranceModel", item.getDescription());


        binding.taiid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MyInsuranceActivity) getActivity()).closeShit();
            }
        });

        binding.laghv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MyInsuranceActivity) getActivity()).closeShit();
            }
        });


        binding.a1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.a1.isChecked()) {
                    binding.a2.setChecked(false);
                    binding.a3.setChecked(false);
                    binding.a4.setChecked(false);
                }
            }
        });
        binding.a2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.a2.isChecked()) {
                    binding.a1.setChecked(false);
                    binding.a3.setChecked(false);
                    binding.a4.setChecked(false);
                }
            }
        });
        binding.a3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.a3.isChecked()) {
                    binding.a2.setChecked(false);
                    binding.a1.setChecked(false);
                    binding.a4.setChecked(false);
                }
            }
        });
        binding.a4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.a4.isChecked()) {
                    binding.a2.setChecked(false);
                    binding.a3.setChecked(false);
                    binding.a1.setChecked(false);
                }
            }
        });


        return view;
    }


}
