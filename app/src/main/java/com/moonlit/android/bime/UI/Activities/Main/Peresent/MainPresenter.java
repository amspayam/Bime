package com.moonlit.android.bime.UI.Activities.Main.Peresent;

import android.content.Context;

import com.google.gson.JsonObject;
import com.moonlit.android.bime.Api.ConstantsMethodServer;
import com.moonlit.android.bime.Api.ServerListener;
import com.moonlit.android.bime.Master.IdentifyModel;
import com.moonlit.android.bime.UI.Activities.Main.Model.MainModel;
import com.moonlit.android.bime.UI.Activities.Main.Model.POJO.SpeechMS;
import com.moonlit.android.bime.UI.Activities.Main.View.MainViewInterface;
import com.moonlit.android.bime.Utils.NetworkUtils;

public class MainPresenter implements MainPresenterInterface, ServerListener {

    private Context context;
    private MainViewInterface mainViewInterface;
    private MainModel mainModel;

    public MainPresenter(Context context, MainViewInterface mainViewInterface) {
        this.mainViewInterface = mainViewInterface;
        this.context = context;
        mainModel = new MainModel();
    }

    public void aiSpeech(String text) {
        if (new NetworkUtils(context).isOnline())
            mainModel.aiSpeechApi(new SpeechMS(text), this);
        else
            mainViewInterface.networkWarning(false);
    }

    @Override
    public void onSuccess(IdentifyModel identify, JsonObject data) {
        switch (identify.getMethodName()) {
            case ConstantsMethodServer.getFirstServerData:
                resultLSpeech(data);
                break;
        }
    }

    @Override
    public void onFailure(IdentifyModel identify, String error) {
        mainViewInterface.showError(error);
    }

    @Override
    public void resultLSpeech(JsonObject data) {
        mainViewInterface.aiSpeechShow(data.get("Result").getAsJsonObject().get("Id").getAsInt());
    }
}
