package com.moonlit.android.bime.Master;

public class IdentifyModel<T> {

    private int methodName;
    private T value = null;

    public IdentifyModel(int methodName) {
        this.methodName = methodName;
    }

    public IdentifyModel(int methodName, T value) {
        this.methodName = methodName;
        this.value = value;
    }

    public int getMethodName() {
        return methodName;
    }

    public void setMethodName(int methodName) {
        this.methodName = methodName;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

}
