package com.moonlit.android.bime.UI.Activities.Inquiry.View;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.moonlit.android.bime.Master.CActivity;
import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.Inquiry.Model.POJO.InquiryMS;
import com.moonlit.android.bime.UI.Activities.Inquiry.Peresent.InquiryPresenter;
import com.moonlit.android.bime.databinding.ActivityInquiryBinding;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import ir.pkokabi.alertview.AlertView;
import ir.pkokabi.pdialogs.DialogGeneral.InternetWarning;

public class InquiryActivity extends CActivity implements InquiryInterface {

    ActivityInquiryBinding binding;
    InquiryPresenter inquiryPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_inquiry);

        initialActivity();
        initialDataActivity();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                binding.firstImgv.setImageURI(result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void networkWarning(boolean showRetry) {
        binding.priceBtn.stopBtnLoading();
        new InternetWarning(context);
    }

    @Override
    public void showError(String message) {
        binding.priceBtn.stopBtnLoading();
        new AlertView(context, message, AlertView.STATE_ERROR);
    }

    @Override
    public void showMessage(String message) {
        binding.priceBtn.stopBtnLoading();
        new AlertView(context, message, AlertView.STATE_SUCCESS) {
            @Override
            public void onFinish() {
                finish();
            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.firstImgv:
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(this);
                break;
            case R.id.priceBtn:
                binding.priceBtn.startBtnLoading();
                inquiryPresenter.estimatePrice(new InquiryMS(binding.insuranceNumberEdt.getEditTextInput()
                        , binding.descEdt.getEditTextInput()));
                break;
        }

    }

    @Override
    protected void initialDataActivity() {
        super.initialDataActivity();

        inquiryPresenter = new InquiryPresenter(context, this);
        toolBarConfig(BACK, DEFAULT_COLOR, "استعلام خسارت");
        setListenerForViews();

    }

    @Override
    public void setListenerForViews() {
        binding.firstImgv.setOnClickListener(this);
        binding.priceBtn.setOnClickListener(this);
    }

}