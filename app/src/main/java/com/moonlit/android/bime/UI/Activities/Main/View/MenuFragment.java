package com.moonlit.android.bime.UI.Activities.Main.View;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moonlit.android.bime.R;
import com.moonlit.android.bime.UI.Activities.BuyInsurance.BuyInsuranceActivity;
import com.moonlit.android.bime.UI.Activities.Inquiry.View.InquiryActivity;
import com.moonlit.android.bime.UI.Activities.MyInsuranceList.View.MyInsuranceActivity;
import com.moonlit.android.bime.databinding.MenuFragmnetBinding;


/**
 * Created by hosein on 6/25/17.
 */

public class MenuFragment extends Fragment implements View.OnClickListener {
    private MenuFragmnetBinding binding;


    public static MenuFragment newInstance() {

        Bundle args = new Bundle();
        MenuFragment fragment = new MenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.menu_fragmnet, container, false);

        binding.buyRlt.setOnClickListener(this);
        binding.myBimeRlt.setOnClickListener(this);
        binding.estelameGheymatRlt.setOnClickListener(this);
        binding.ertebatBaKarshenas.setOnClickListener(this);
        binding.tamasBaMaRtly.setOnClickListener(this);
        binding.khorojRtly.setOnClickListener(this);
        binding.closeACImgB.setOnClickListener(this);

        return binding.getRoot();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buyRlt:
                startActivity(new Intent(getActivity(), BuyInsuranceActivity.class));
                break;
            case R.id.myBimeRlt:
                startActivity(new Intent(getActivity(), MyInsuranceActivity.class));
                break;
            case R.id.estelameGheymatRlt:
                startActivity(new Intent(getActivity(), InquiryActivity.class));
                break;
            case R.id.ertebatBaKarshenas:

                break;
            case R.id.tamasBaMaRtly:

                break;
            case R.id.khorojRtly:

                break;
            case R.id.closeACImgB:

                break;

        }
        ((MainActivity) getActivity()).closeShit();
    }
}
