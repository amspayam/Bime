package com.moonlit.android.bime.Api.ApiName;

import com.moonlit.android.bime.Api.ModelServer.ResponseBaseM;
import com.moonlit.android.bime.UI.Activities.Accident.Model.POJO.AddSOSMs;
import com.moonlit.android.bime.UI.Activities.Inquiry.Model.POJO.InquiryMS;
import com.moonlit.android.bime.UI.Activities.Main.Model.POJO.SpeechMS;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface GeneralApiInterface {

    @POST("SOS/AddSOS")
    Call<ResponseBaseM> addSOS(@Body AddSOSMs sosModel);

    @POST("Bime/GetStimateedBimeValue")
    Call<ResponseBaseM> estimatePrice(@Body InquiryMS inquiryMS);

    @GET("bime/GetMyBime?token=aminsasdasdd")
    Call<ResponseBaseM> getMyBime();

    @POST("user/callhelp")
    Call<ResponseBaseM> aiSpeech(@Body SpeechMS speechModel);
}